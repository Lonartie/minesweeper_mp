#include <boost/test/unit_test.hpp>
#include "../MMPCore/Serializable.h"

using namespace MMPCore::Serialization;

BOOST_AUTO_TEST_SUITE(TS_Serialization)

BOOST_AUTO_TEST_CASE(Serializing_simple_struct)
{
   class simple: public Serializable
   {
      SERIALIZABLE_CLASS;
   public:
      SERIALIZABLE(int, a);
      SERIALIZABLE(double, b) = 2.5;
      SERIALIZABLE_ARRAY(int, c, 3) = {1, 2, 3};
   };

   simple test;
   auto data1 = test.__Save();
   test.c[1] = 9;
   auto data2 = test.__Save();

   BOOST_CHECK_EQUAL(true, test.__CanLoadFrom(data1));
   BOOST_CHECK_EQUAL(true, test.__CanLoadFrom(data2));

   test.__LoadFrom(data1);
   BOOST_CHECK_EQUAL(2, test.c[1]);

   test.__LoadFrom(data2);
   BOOST_CHECK_EQUAL(9, test.c[1]);

   BOOST_CHECK_EQUAL(simple().__TypeHash(), simple().__TypeHash());
}

BOOST_AUTO_TEST_CASE(Serializing_recursive_struct)
{
   class c1: public Serializable
   {
      SERIALIZABLE_CLASS;
   public:
      SERIALIZABLE(int, u) = 1;
   };

   class c2: public Serializable
   {
      SERIALIZABLE_CLASS;
   public:
      SERIALIZABLE(c1, u);
   };

   c2 test;
   auto data1 = test.__Save();
   test.u.u = 9;
   auto data2 = test.__Save();

   BOOST_CHECK_EQUAL(true, test.__CanLoadFrom(data1));
   BOOST_CHECK_EQUAL(true, test.__CanLoadFrom(data2));
   
   test.__LoadFrom(data1);
   BOOST_CHECK_EQUAL(1, test.u.u);

   test.__LoadFrom(data2);
   BOOST_CHECK_EQUAL(9, test.u.u);

   BOOST_CHECK_EQUAL(c2().__TypeHash(), c2().__TypeHash());
   BOOST_CHECK_EQUAL(c1().__TypeHash(), c1().__TypeHash());
   BOOST_CHECK(c1().__TypeHash() != c2().__TypeHash());
}

BOOST_AUTO_TEST_CASE(Serializing_recursive_max_struct)
{
   class c1: public Serializable
   {
      SERIALIZABLE_CLASS;
   public:
      SERIALIZABLE(int, u) = 1;
   };

   class c2: public Serializable
   {
      SERIALIZABLE_CLASS;
   public:
      SERIALIZABLE(c1, u);
   };

   class c3: public Serializable
   {
      SERIALIZABLE_CLASS;
   public:
      SERIALIZABLE(c1, u1);
      SERIALIZABLE(c2, u2);
   };

   c3 test;
   auto data1 = test.__Save();
   test.u2.u.u = 9;
   test.u1.u = 9;
   auto data2 = test.__Save();

   BOOST_CHECK_EQUAL(true, test.__CanLoadFrom(data1));
   BOOST_CHECK_EQUAL(true, test.__CanLoadFrom(data2));

   test.__LoadFrom(data1);
   BOOST_CHECK_EQUAL(1, test.u1.u);
   BOOST_CHECK_EQUAL(1, test.u2.u.u);

   test.__LoadFrom(data2);
   BOOST_CHECK_EQUAL(9, test.u1.u);
   BOOST_CHECK_EQUAL(9, test.u2.u.u);

   BOOST_CHECK_EQUAL(c3().__TypeHash(), c3().__TypeHash());
   BOOST_CHECK_EQUAL(c2().__TypeHash(), c2().__TypeHash());
   BOOST_CHECK_EQUAL(c1().__TypeHash(), c1().__TypeHash());
   BOOST_CHECK(c1().__TypeHash() != c2().__TypeHash());
   BOOST_CHECK(c1().__TypeHash() != c3().__TypeHash());
   BOOST_CHECK(c2().__TypeHash() != c3().__TypeHash());
}

BOOST_AUTO_TEST_SUITE_END()