#pragma once

#define QT_QUEUED_BEGIN                                                             \
QCoreApplication app(boost::unit_test::framework::master_test_suite().argc,         \
                     boost::unit_test::framework::master_test_suite().argv);        \
                                                                                    \
std::thread thread_server([&](void)                                                 \
{                                                                                   
                                                                                    
                                                                                    
#define QT_QUEUED_END                                                               \
                                                                                    \
   QCoreApplication::quit();                                                        \
});                                                                                 \
                                                                                    \
BOOST_CHECK(app.exec() == 0);                                                       \
thread_server.join();
