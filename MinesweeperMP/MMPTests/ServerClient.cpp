#include <boost/test/unit_test.hpp>

#include <QCoreApplication>

#include <chrono>

#include "TestUtils.h"

#include "../MMPCore/Server.h"
#include "../MMPCore/Client.h"
#include "../MMPCore/Friend.h"

#include <thread>

using namespace MMPCore::Networking;
using namespace MMPCore::Identity;

BOOST_AUTO_TEST_SUITE(TS_Networking)


BOOST_AUTO_TEST_CASE(ClientCanConnectToServer)
{
   using namespace std;
   using namespace std::chrono;

   Server server(2555);
   Client client;
   QString received = "";
   
   QObject::connect(&server, &Server::ClientConnected, [&server](auto socket) { server.SendData(socket, QString("Hello").toLatin1()); });
   QObject::connect(&client, &Client::DataReceived, [&received](auto data) { received = QString(data); });

   client.Connect("127.0.0.1", 2555);
}

BOOST_AUTO_TEST_SUITE_END()