#pragma once

#include "stdafx.h"
#include "Serializable.h"

namespace MMPCore
{
	namespace Mechanics
	{
		namespace Game
		{
			class MMPCORE_EXPORT Board : public MMPCore::Serialization::Serializable 
			{
				MEMORY(Board);
            SERIALIZABLE_CLASS;

         public:

            static constexpr std::size_t MaxBoardWidth = 64;
            static constexpr std::size_t MaxBoardHeight = 64;
            static constexpr std::size_t MaxBoardSize = MaxBoardWidth * MaxBoardHeight;

				enum FieldState
				{
					CLOSED			= (1 << 0),
					OPEN				= (1 << 1),
					HAS_MINE			= (1 << 2),
					NO_MINE			= (1 << 3),
				};

				Board(int boardWidth, int boardHeight);
            Board();
				virtual ~Board();

				FieldState GetFieldState(std::size_t x, std::size_t y) const;
				void SetFieldState(std::size_t x, std::size_t y, FieldState state);
				void SetFieldState(std::size_t x, std::size_t y, int state);
				std::size_t GetBoardWidth() const;
				std::size_t GetBoardHeight() const;
            void SetBoardSize(std::size_t w, std::size_t h);
				int CountMines(std::size_t x, std::size_t y) const;

				bool FieldStateContains(std::size_t x, std::size_t y, FieldState state) const;

				const uchar* cbegin() const;
				const uchar* cend() const;

				uchar* begin();
				uchar* end();

			private:

				SERIALIZABLE_ARRAY(uchar, m_boardStates, MaxBoardSize);
				SERIALIZABLE(std::size_t, m_actualBoardWidth) = 8;
				SERIALIZABLE(std::size_t, m_actualBoardHeight) = 16;

				std::size_t GetActualBoardSize() const;

			};

		}
	}
}
