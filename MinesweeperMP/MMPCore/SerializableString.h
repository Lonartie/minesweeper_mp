#pragma once

#include "stdafx.h"
#include "Serializable.h"

namespace MMPCore
{
   namespace Serialization
   {
      namespace Types
      {
         template<std::size_t MAX_SPACE>
         class SerializableString: public MMPCore::Serialization::Serializable
         {
            MEMORY(SerializableString);
            SERIALIZABLE_CLASS;

         public:

            static constexpr std::size_t Bytes = MAX_SPACE;

            SerializableString();
            SerializableString(const QString& str);
            template<std::size_t SPACE> SerializableString(const SerializableString<SPACE>& str);
            template<std::size_t SPACE> SerializableString(SerializableString<SPACE>&& str);
            virtual ~SerializableString();

            QString ToQString() const;
            void SetQString(const QString& str);
            void Clean();
            template<std::size_t SPACE> SerializableString<SPACE> Trim() const;

            Serializable& operator=(const QString& str);
            Serializable& operator=(QString&& str);
            template<std::size_t SPACE> Serializable& operator=(const SerializableString<SPACE>& str);
            template<std::size_t SPACE> Serializable& operator=(SerializableString<SPACE>&& str);
            Serializable& operator=(const SerializableString& str);
            Serializable& operator=(SerializableString&& str);

            bool operator==(const QString& str) const noexcept;
            template<std::size_t SPACE> bool operator==(const SerializableString<SPACE>& str) const noexcept;

            operator QString() const noexcept;

            std::size_t Size();
            const char* const Data() const;

         private:

            SERIALIZABLE_ARRAY(char, m_data, Bytes);

         };

         template<std::size_t MAX_SPACE>
         Serializable& MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator=(SerializableString&& str)
         {
            Clean();
            std::memmove(m_data, str.m_data, Bytes);
            return *this;
         }

         template<std::size_t MAX_SPACE>
         Serializable& MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator=(const SerializableString& str)
         {
            Clean();
            std::memcpy(m_data, str.m_data, Bytes);
            return *this;
         }

         template<std::size_t MAX_SPACE>
         inline MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator QString() const noexcept
         {
            return ToQString();
         }

         template<std::size_t MAX_SPACE>
         inline const char* const MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::Data() const
         {
            return m_data;
         }

         template<std::size_t MAX_SPACE>
         inline std::size_t MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::Size()
         {
            std::size_t size;
            for (std::size_t i = 0; i < Bytes; i++)
               if (m_data[size++] == '\0')
                  return size;
            return size;
         }

         template<std::size_t MAX_SPACE>
         template<std::size_t SPACE>
         inline SerializableString<SPACE> MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::Trim() const
         {
            SerializableString<SPACE> str;
            str = *this;
            return str;
         }

         template<std::size_t MAX_SPACE>
         template<std::size_t SPACE>
         inline MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::SerializableString(SerializableString<SPACE>&& str)
         {
            Clean();
            std::memmove(m_data, str.m_data, std::min<std::size_t>(MAX_SPACE, SPACE));
         }

         template<std::size_t MAX_SPACE>
         template<std::size_t SPACE>
         inline MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::SerializableString(const SerializableString<SPACE>& str)
         {
            Clean();
            std::memcpy(m_data, str.m_data, std::min<std::size_t>(MAX_SPACE, SPACE));
         }

         template<std::size_t MAX_SPACE>
         template<std::size_t SPACE>
         inline bool MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator==(const SerializableString<SPACE>& str) const noexcept
         {
            static_assert(MAX_SPACE == SPACE, "Can only compare string of the same base size! Try the trim function.");

            return std::equal(m_data, m_data + Bytes, str.m_data, str.m_data + Bytes);
         }

         template<std::size_t MAX_SPACE>
         template<std::size_t SPACE>
         inline Serializable& MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator=(SerializableString<SPACE>&& str)
         {
            Clean();
            std::memmove(m_data, str.m_data, std::min<std::size_t>(MAX_SPACE, SPACE));
            return *this;
         }

         template<std::size_t MAX_SPACE>
         template<std::size_t SPACE>
         inline Serializable& MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator=(const SerializableString<SPACE>& str)
         {
            Clean();
            std::memcpy(m_data, str.Data(), std::min<std::size_t>(MAX_SPACE, SPACE));
            return *this;
         }

         template<std::size_t MAX_SPACE>
         inline void MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::Clean()
         {
            std::fill(m_data, m_data + Bytes, '\0');
         }

         template<std::size_t MAX_SPACE>
         inline bool MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator==(const QString& str) const noexcept
         {
            if (MAX_SPACE != str.size())
               return false;

            return std::equal(m_data, m_data + Bytes, str.m_data, str.m_data + Bytes);
         }

         template<std::size_t MAX_SPACE>
         inline Serializable& MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator=(QString&& str)
         {
            Clean();
            std::memmove(m_data, str.toLocal8Bit().data(), std::min<std::size_t>(MAX_SPACE, str.size()));
            return *this;
         }

         template<std::size_t MAX_SPACE>
         inline Serializable& MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::operator=(const QString& str)
         {
            Clean();
            std::memcpy(m_data, str.toLocal8Bit().data(), std::min<std::size_t>(MAX_SPACE, str.size()));
            return *this;
         }

         template<std::size_t MAX_SPACE>
         inline void MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::SetQString(const QString& str)
         {
            Clean();
            auto std_string = str.toStdString();
            for (int i = 0; i < std::min<std::size_t>(str.size(), Bytes); i++)
               m_data[i] = std_string[i];
         }

         template<std::size_t MAX_SPACE>
         inline QString MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::ToQString() const
         {
            return QString(QByteArray(m_data, Bytes));
         }

         template<std::size_t MAX_SPACE>
         inline MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::SerializableString(const QString& str)
         {
            Clean();
            SetQString(str);
         }

         template<std::size_t MAX_SPACE>
         inline MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::SerializableString()
         {
            Clean();
         }

         template<std::size_t MAX_SPACE>
         inline MMPCore::Serialization::Types::SerializableString<MAX_SPACE>::~SerializableString()
         {
            Clean();
         }
      }
   }
}
