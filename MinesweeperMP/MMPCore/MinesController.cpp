#include "MinesController.h"

namespace
{
	double random(double min, double max)
	{
		return rand() / static_cast<double>(RAND_MAX) * (max - min) + min;
	}
}

MMPCore::Mechanics::Game::MinesController::MinesController(PlayMode mode /*= Easy*/)
	: m_playMode(mode)
{}

MMPCore::Mechanics::Game::MinesController::MinesController()
{}

MMPCore::Mechanics::Game::MinesController::~MinesController()
{}

void MMPCore::Mechanics::Game::MinesController::InitBoard(Board& board)
{
	auto factor = percentages[static_cast<int>(m_playMode)];
	auto width = board.GetBoardWidth();
	auto height = board.GetBoardHeight();
	auto fc = 0;

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			fc = random(0, 100);

			if (fc <= factor)
			{
				board.SetFieldState(x, y, Board::CLOSED | Board::HAS_MINE);
			}
			else
			{
				board.SetFieldState(x, y, Board::CLOSED | Board::NO_MINE);
			}
		}
	}
}
