#pragma once

// MMPCore
#include "stdafx.h"
#include "AbstractStorage.h"

namespace MMPCore
{
	namespace Mechanics
	{
		namespace Storage
		{
			class MMPCORE_EXPORT MemoryStorage: public MMPCore::Mechanics::Storage::AbstractStorage
			{
				MEMORY(MemoryStorage);

			public:
				using AbstractStorage::Save;

				MemoryStorage(QObject *parent = Q_NULLPTR);
				virtual ~MemoryStorage();

         public /*virtual*/:

            virtual bool Save(const QString& key, const QByteArray& data) override;
            virtual void Remove(const QString& key) override;
            virtual QByteArray Load(const QString& key) const override;
            virtual Common::Linq<QString> GetKeys() const override;
            virtual void Clear() noexcept override;

			private:

				Common::Linq<std::pair<QString /*key*/, QByteArray /*value*/>> m_data;

			};

		}
	}
}