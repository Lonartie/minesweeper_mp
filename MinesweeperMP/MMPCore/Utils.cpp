#include "Utils.h"

std::size_t MMPCore::Utils::RandomGUID()
{
   return static_cast<std::size_t>(rand()) * static_cast<std::size_t>(rand()) + static_cast<std::size_t>(rand());
}
