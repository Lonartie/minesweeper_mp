//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#ifndef BaseLinq_H
#define BaseLinq_H

#include "AbstractLinq.h"
#include "LinqStructs.h"

#include <QVector>
#include <QStringList>

#include <vector>
#include <exception>

namespace Common
{
   template<typename T>
   struct BaseLinq: public LinqData<T>
   {
      using iterator = typename std::vector<T>::iterator;
      using size_type = std::size_t;
      using index_to_t = T(*)(size_type);
      using type = T;
      using value_type = type;

      BaseLinq(const std::vector<T>& list)
         : m_list(list)
      {}

      BaseLinq(std::vector<T>&& list)
         : m_list(std::move(list))
      {}

      BaseLinq(const std::initializer_list<T>& list)
         : m_list(list)
      {}

      BaseLinq(std::initializer_list<T>&& list)
         : m_list(std::move(list))
      {}

      BaseLinq(const QVector<T>& list)
         : m_list(list.toStdVector())
      {}

      BaseLinq(QVector<T>&& list)
         : m_list(std::move(list.toStdVector()))
      {}

      BaseLinq(const QStringList& list)
         : m_list(list.toVector().toStdVector())
      {}

      BaseLinq(T* begin, T* end)
         : m_list(std::vector<T>(begin, end))
      {}

      BaseLinq(const LinqData<T>& linq)
         : m_list(linq.ConstList())
      {}

      template<typename U>
      BaseLinq(const BaseLinq<U>& list)
         : m_list()
      {
         static_assert(std::is_convertible<T, U>::value, "T is not constructible from U");
         for (const U& item : list.Get())
            m_list.emplace_back(item);
      }

      BaseLinq(Linq<T>&& list) noexcept
         : m_list(std::move(list.m_list))
      {}

      BaseLinq() = default;

      BaseLinq(const BaseLinq<T>&) = default;

      //~BaseLinq() = default;

      /// @brief 							get the size of this BaseLinq
      /// @note  							is const
      size_type size() const
      {
         return m_list.size();
      }

      /// @brief 							clears the list
      /// @note  							after calling this function, no items are in this BaseLinq anymore!
      Linq<T>& Clear()
      {
         m_list.clear();
         return ToLinq<T>(this);
      }

      /// @brief 							get the data as vector
      /// @note  							result is const reference
      ///                           function is const
      const std::vector<T>& Get() const
      {
         return m_list;
      }

      /// @brief 							get the original data vector
      /// @note  							modifying the result modifies this BaseLinq content
      ///                           result is reference
      std::vector<T>& Original()
      {
         return m_list;
      }

      /// @brief 							get a copy of this BaseLinq object
      /// @note  							function is const
      Linq<T> Copy() const
      {
         return Linq<T>(m_list);
      }

      /// @brief 							get the raw data pointer
      /// @note  							modifying the pointer data modifies this BaseLinq content
      ///                           only available if T is not bool
      T* Raw()
      {
         static_assert(!std::is_same<T, bool>::value, "This function cannot be used with T=bool");
         return m_list.data();
      }

      /// @brief 							count the number of occurrences in this BaseLinq
      size_type Count(T obj) const
      {
         size_type count = 0;

         for (const auto& item : m_list)
         {
            if (item == obj)
            {
               count++;
            }
         }

         return count;
      }

      /// @brief 							checks whether or not a given item exists in list
      bool Contains(T obj) const
      {
         for (const auto& item : m_list)
         {
            if (item == obj)
            {
               return true;
            }
         }
         return false;
      }

      /// @brief 							get a new BaseLinq object with different type than this one
      /// @note  							creates a copy!
      ///                           tries to static_cast every item in list
      template<typename U>
      Linq<U> ChangeType() const
      {
         static_assert(std::is_convertible<T, U>::value, "T cannot be converted to U!");
         return Linq<U>(*this);
      }

      /// @brief 							reverse the order of objects in this list
      /// @note  							creates NO copy, returns itself
      Linq<T>& Reverse()
      {
         std::reverse(m_list.begin(), m_list.end());
         return ToLinq<T>(this);
      }

      /// @brief 							resizes the list to \a size
      /// @note  							creates NO copy, returns itself
      Linq<T>& Resize(size_type size)
      {
         m_list.resize(size);
         return ToLinq<T>(this);
      }

      Linq<T>&& Move()
      {
         return std::move(ToLinq<T>(this));
      }

      /// @brief 							adds an element to this BaseLinq object
      /// @note  							creates no copy, returns itself
      Linq<T>& Add(const T& obj)
      {
         m_list.emplace_back(obj);
         return ToLinq<T>(this);
      }
      Linq<T>& Add(T&& obj)
      {
         m_list.emplace_back(std::move(obj));
         return ToLinq<T>(this);
      }

      /// @brief 							adds an element to this BaseLinq object at the specified index
      /// @note  							creates no copy, returns itself
      Linq<T>& AddAt(size_type index, const T& obj)
      {
         auto pos = m_list.begin() + index;

         if (pos < m_list.begin() || pos > m_list.end())
            throw std::runtime_error(("index [" + std::to_string(index) + "] is invalid").c_str());

         m_list.insert(pos, obj);
         return ToLinq<T>(this);
      }
      Linq<T>& AddAt(size_type index, T&& obj)
      {
         auto pos = m_list.begin() + index;

         if (pos < m_list.begin() || pos > m_list.end())
            throw std::runtime_error(("index [" + std::to_string(index) + "] is invalid").c_str());

         m_list.insert(pos, std::move(obj));
         return ToLinq<T>(this);
      }

      /// @brief 							removes the given object from the BaseLinq if it exists
      /// @note  							creates no copy, returns itself
      Linq<T>& Remove(const T& obj)
      {
         auto iter = std::find<iterator, T>(m_list.begin(), m_list.end(), obj);

         if (iter != m_list.end())
            m_list.erase(iter);

         return ToLinq<T>(this);
      }

      /// @brief 							removes the object at the given index
      /// @note  							creates NO copy, returns itself
      Linq<T>& RemoveAt(size_type index)
      {
         auto pos = m_list.begin() + index;

         if (pos < m_list.begin() || pos > m_list.end())
            throw std::runtime_error(("index [" + std::to_string(index) + "] is invalid").c_str());

         m_list.erase(pos);

         return ToLinq<T>(this);
      }

      /// @brief 							filters the BaseLinq by the given function
      /// @note  							creates no copy, returns itself
      /// @note  							erases data!
      /// @note  							requires function signature like: bool ([const] T [&])
      template<typename FNC, typename ...DefaultArgs>
      Linq<T>& Filter(FNC pred, DefaultArgs...args)
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));

         static_assert(std::is_convertible<out_type, bool>::value, "the return type has to be a convertible to bool!");

         for (long long i = m_list.size() - 1; i >= 0; i--)
         {
            if (!fnc(m_list[i]))
            {
               m_list.erase(m_list.begin() + i);
            }
         }

         return ToLinq<T>(this);
      }

      /// @brief 							filters the BaseLinq by the given function
      /// @note  							creates no copy, returns itself
      /// @note  							erases data!
      /// @note  							requires function signature like: bool ([const] T [&])
      template<typename FNC, typename ...DefaultArgs>
      Linq<T> Where(FNC pred, DefaultArgs...args) const
      {
         auto fnc = [&](const T& obj)
         {
            return std::invoke(pred, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         static_assert(std::is_convertible<out_type, bool>::value, "the return type has to be a convertible to bool!");

         Linq<T> result;

         for (const auto& item : m_list)
         {
            if (fnc(item))
            {
               result.Original().emplace_back(item);
            }
         }

         return result;
      }

      /// @brief 							executes function on each object in this BaseLinq and returns itself
      /// @note  							creates NO copy! returns itself
      template< typename FNC, typename ...DefaultArgs>
      Linq<T>& SelectInPlace(FNC pred, DefaultArgs...args)
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));

         static_assert(std::is_convertible<out_type, T>::value, "the return type has to convertible to T!");

#pragma omp parallel for
         for (size_type i = 0; i < m_list.size(); i++)
            m_list[i] = std::move(fnc(m_list[i]));

         return ToLinq<T>(this);
      }

      /// @brief 							creates a new BaseLinq object where the result of the function executed on each object is stored in
      /// @note  							creates a copy!
      template<typename FNC, typename ...DefaultArgs>
      auto Select(FNC pred, DefaultArgs...args) const
      {
         auto fnc = [&](const T& obj)
         {
            return std::invoke(pred, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));

         Linq<out_type> result;
         result.Resize(m_list.size());

         for (std::size_t i = 0; i < m_list.size(); i++)
         {
            result.Original()[i] = std::move(fnc(m_list[i]));
         }

         return result;
      }

      /// @brief 							creates a new BaseLinq object where the result of the function executed on each object is stored in
      /// @note  							creates a copy! function is const
      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      auto SelectIf(FNC pred1, FNC2 pred2, DefaultArgs... args) const
      {
         auto fnc = [&](const T& obj)
         {
            return std::invoke(pred1, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         auto sel = [&](out_type& obj)
         {
            return std::invoke(pred2, obj);
         };
         using sel_type = decltype(sel(*(out_type*) (0)));

         static_assert(std::is_convertible<sel_type, bool>::value, "the return type has to be a convertible to bool!");

         Linq<out_type> result;

         for (const T& item : m_list)
         {
            auto selection = std::move(fnc(item));
            if (sel(selection))
               result.Original().emplace_back(selection);
         }

         return result;
      }

      /// @brief 							executes function on each object in this BaseLinq and returns itself
      /// @note  							creates NO copy! returns itself
      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      Linq<T>& SelectInPlaceIf(FNC pred1, FNC2 pred2, DefaultArgs... args)
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred1, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         auto sel = [&](out_type& obj)
         {
            return std::invoke(pred2, obj);
         };
         using sel_type = decltype(sel(*(out_type*) (0)));

         static_assert(std::is_convertible<sel_type, bool>::value, "the return type has to be a convertible to bool!");
         static_assert(std::is_same<T, out_type>::value, "the return type has to be T!");

         for (size_type i = 0; i < m_list.size(); i++)
         {
            auto selection = std::move(fnc(m_list[i]));

            if (sel(selection))
            {
               m_list[i] = std::move(selection);
            }
            else
            {
               m_list.erase(m_list.begin() + i);
               i--;
            }
         }

         return ToLinq<T>(this);
      }


      /// @brief 							creates a new BaseLinq object where the result of the function executed on each object is stored in
      /// @note  							creates a copy! function is const
      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      auto IfSelect(FNC pred1, FNC2 pred2, DefaultArgs... args) const
      {
         auto fnc = [&](const T& obj)
         {
            return std::invoke(pred2, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         auto sel = [&](const T& obj)
         {
            return std::invoke(pred1, obj);
         };
         using sel_type = decltype(sel(*(T*) (0)));

         static_assert(std::is_convertible<sel_type, bool>::value, "the return type has to be a convertible to bool!");

         Linq<out_type> result;

         for (const T& item : m_list)
         {
            if (sel(item))
               result.Original().emplace_back(std::move(fnc(item)));
         }

         return result;
      }

      /// @brief 							executes function on each object in this BaseLinq and returns itself
      /// @note  							creates NO copy! returns itself
      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      Linq<T>& IfSelectInPlace(FNC pred1, FNC2 pred2, DefaultArgs... args)
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred2, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         auto sel = [&](const T& obj)
         {
            return std::invoke(pred1, obj);
         };
         using sel_type = decltype(sel(*(T*) (0)));

         static_assert(std::is_convertible<sel_type, bool>::value, "the return type has to be a convertible to bool!");
         static_assert(std::is_same<T, out_type>::value, "the return type has to be T!");

         for (size_type i = 0; i < m_list.size(); i++)
         {
            if (sel(m_list[i]))
            {
               m_list[i] = std::move(fnc(m_list[i]));
            }
            else
            {
               m_list.erase(m_list.begin() + i);
               i--;
            }
         }

         return ToLinq<T>(this);
      }

      /// @brief 							accumulates every object from this list into one single object
      /// @note  							begin is reference, take it as such or else you'll have default output
      /// @note  							requires function signature like: void (V&, [const] T&)
      template<typename V, typename FNC, typename ...DefaultArgs>
      V Accumulate(V begin, FNC pred, DefaultArgs... args) const
      {
         auto fnc = [&](V& begin, const T& obj)
         {
            return std::invoke(pred, begin, obj, args...);
         };

         for (const T& item : m_list)
            fnc(begin, item);

         return begin;
      }

      /// @brief 							merges two BaseLinqs into one where the other one is places at \a index
      /// @note  							creates NO copy! returns itself
      template<typename U>
      Linq<T>& Merge(const Linq<U>& other, size_type index)
      {
         static_assert(std::is_convertible<U, T>::value, "U is not convertible to T!");
         m_list.insert(m_list.begin() + index, other.Get().begin(), other.Get().end());

         return ToLinq<T>(this);
      }

      /// @brief 							merges two BaseLinqs into one where the other one is places at the end of this one
      /// @note  							creates NO copy! returns itself
      template<typename U>
      Linq<T>& Merge(const Linq<U>& other)
      {
         static_assert(std::is_convertible<U, T>::value, "U is not convertible to T!");
         m_list.insert(m_list.end(), other.Get().begin(), other.Get().end());

         return ToLinq<T>(this);
      }

      /// @brief 							merges two BaseLinqs into one where the other one is places at the end of this one
      /// @note  							creates NO copy! returns itself
      template<typename U>
      Linq<T>& Merge(Linq<U>&& other)
      {
         static_assert(std::is_convertible<U, T>::value, "U is not convertible to T!");
         m_list.insert(m_list.end(), std::make_move_iterator(other.Get().begin()), std::make_move_iterator(other.Get().end()));

         return ToLinq<T>(this);
      }

      /// @brief 							creates a new BaseLinq object where every \a count elements from \a begin are stored in
      /// @note  							creates a copy! function is const
      Linq<T> GetRange(size_type begin, size_type count) const
      {
         return Linq<T>(std::vector<T>(m_list.begin() + begin, m_list.begin() + begin + count));
      }

      /// @brief 							creates a new BaseLinq object where the first \a count elements are stored in
      /// @note  							creates a copy! function is const
      Linq<T> Left(size_type count) const
      {
         return GetRange(0, count);
      }

      /// @brief 							creates a new BaseLinq object where the last \a count elements are stored in
      /// @note  							creates a copy! function is const
      Linq<T> Right(size_type count) const
      {
         return GetRange(m_list.size() - count, count);
      }

      /// @brief 							applies a function on each element
      /// @note  							creates NO copy! returns itself
      template<typename FNC, typename ...DefaultArgs>
      Linq<T>& Foreach(FNC pred, DefaultArgs...args)
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred, obj, args...);
         };
         for (T& item : m_list)
            fnc(item);
         return ToLinq<T>(this);
      }

      /// @brief 							applies a function on each element
      /// @note  							creates NO copy! returns itself
      template<typename FNC, typename ...DefaultArgs>
      const Linq<T>& Foreach(FNC pred, DefaultArgs...args) const
      {
         auto fnc = [&](const T& obj)
         {
            return std::invoke(pred, obj, args...);
         };
         for (const T& item : m_list)
            fnc(item);
         return ToConstLinq<T>(this);
      }

      /// @brief 							sorts all elements by \a comp function
      /// @note  							creates no copy! returns itself
      template<typename FNC>
      Linq<T>& Sort(FNC comp)
      {
         std::sort(m_list.begin(), m_list.end(), comp);
         return ToLinq<T>(this);
      }

      /// @brief 							sorts all elements by default compare function
      /// @note  							creates no copy! returns itself
      Linq<T>& Sort()
      {
         std::sort(m_list.begin(), m_list.end());
         return ToLinq<T>(this);
      }

      /// @brief 							fill this list with \a obj \a count times
      /// @note  							creates NO copy! returns itself
      Linq<T>& Fill(const T& obj, size_type count)
      {
         for (size_type i = 0; i < count; i++)
            m_list.emplace_back(obj);
         return ToLinq<T>(this);
      }

#pragma warning(push)
#pragma warning(disable : 4267)
      /// @brief 							iterates from 0 to \a count and stores result of \a pred (current_index) in this BaseLinq
      /// @note  							creates NO copy! returns itself
      template<typename FNC, typename ...DefaultArgs>
      Linq<T>& FillRangeFunction(size_type count, FNC pred, DefaultArgs... args)
      {
         auto fnc = [&](size_type num)
         {
            return std::invoke(pred, num, args...);
         };

         using out_type = decltype(fnc(*(size_type*) (0)));
         static_assert(std::is_convertible<out_type, T>::value, "the predicate has to be a convertible to T!");

         for (size_type i = 0; i < count; i++)
            m_list.emplace_back(fnc(i));

         return ToLinq<T>(this);
      }
#pragma warning(pop)

      /// @brief 							iterates from 0 to \a count and stores current_index in this BaseLinq
      /// @note  							creates NO copy! returns itself
      Linq<T>& FillRange(size_type count)
      {
         for (size_type i = 0; i < count; i++)
            m_list.emplace_back(OneToOne(i));
         return ToLinq<T>(this);
		}

		/// @brief 							creates a new Linq where every item occurs is exactly one time
		Linq<T>& Distinct()
		{
			for( int i = 0; i < m_list.size(); i++)
			{
				if (Count(m_list[i]) > 1)
				{
					RemoveAt(i--);
				}
			}

			return ToLinq<T>(this);
		}

      /// @brief 							creates a new Linq where every item occurs is exactly one time
      Linq<T> Distinct() const
      {
         Linq<T> result;

         for (const auto& item : m_list)
         {
            if (!result.Contains(item))
            {
               result.Add(item);
            }
         }

         return result;
      }

      /// @brief 							iterates from \a begin to \a end with an increment of \a increment and stores current_index in this BaseLinq
      /// @note  							creates NO copy! returns itself
      /// @note  							begin is inclusive, end is exclusive!
      Linq<T>& FillRange(const T& begin, const T& end, const T& increment)
      {
         for (T i = begin; i < end; i += increment)
            m_list.emplace_back(i);
         return ToLinq<T>(this);
      }

      /// @brief 							returns the index of the first element in list that is equal to \a obj
      size_type IndexOf(const T& obj) const
      {
         return std::find(m_list.begin(), m_list.end(), obj) - m_list.begin();
      }

      /// @brief 							returns the index of the first element in list which address is equal to the address of \a obj
      size_type IndexOfRef(const T& obj) const
      {
         for (size_type i = 0; i < m_list.size(); i++)
            if (&m_list[i] == &obj)
               return i;
         return m_list.size();
      }

      /// @brief 							returns the first element in list or T()
      /// @note  							requirements:  default constructor
      T FirstOrDefault() const
      {
         if (m_list.size() != 0)
            return m_list[0];
         return T{};
      }

      /// @brief 							returns the first element in list
      T First() const
      {
         return m_list[0];
      }

      /// @brief 							returns the first element in list or \a out
      T FirstOr(T out) const
      {
         if (m_list.size() != 0)
            return m_list[0];
         return out;
      }

      /// @brief 							returns the first element in list or throws \a ex
      T FirstOrThrow(std::exception& ex) const
      {
         if (m_list.size() != 0)
            return m_list[0];
         throw ex;
      }

      /// @brief 							returns the first element in list or throws \a ex
      T FirstOrThrow(std::exception&& ex) const
      {
         if (m_list.size() != 0)
            return m_list[0];
         throw ex;
      }

      T AtOrDefault(size_type index) const
      {
         if (m_list.size() > index)
            return m_list[index];
         return T{};
      }

      T At(size_type index) const
      {
         return m_list[index];
      }

      T AtOr(size_type index, T out) const
      {
         if (m_list.size() > index)
            return m_list[index];
         return out;
      }

      T AtOrThrow(size_type index, std::exception& ex) const
      {
         if (m_list.size() > index)
            return m_list[index];
         throw ex;
      }

      T AtOrThrow(size_type index, std::exception&& ex) const
      {
         if (m_list.size() > index)
            return m_list[index];
         throw ex;
      }

      /// @brief 							returns the last element in list or T()
      /// @note  							requirements:  default constructor
      T LastOrDefault() const
      {
         if (m_list.size() != 0)
            return m_list[m_list.size() - 1];
         return T{};
      }

      /// @brief 							returns the last element in list
      T Last() const
      {
         return m_list[m_list.size() - 1];
      }

      /// @brief 							returns the last element in list or \a out
      T LastOr(T out) const
      {
         if (m_list.size() != 0)
            return m_list[m_list.size() - 1];
         return out;
      }

      /// @brief 							returns the last element in list or throws \a ex
      T LastOrThrow(std::exception& ex) const
      {
         if (m_list.size() != 0)
            return m_list[m_list.size() - 1];
         throw ex;
      }

      /// @brief 							returns the last element in list or throws \a ex
      T LastOrThrow(std::exception&& ex) const
      {
         if (m_list.size() != 0)
            return m_list[m_list.size() - 1];
         throw ex;
      }

      /// @brief 							whether or not there are any elements in list (size != 0)
      bool Any()
      {
         return m_list.size() != 0;
      }

      template<typename U>
      bool IsLinqGroup()
      {
         return dynamic_cast<U*>(&this->This()) != nullptr;
      }

      /// @brief 							consider Linq of T and Linq of U, returns the equality of T and U
      template<typename U>
      bool IsSameType(const Linq<U>& other) const
      {
         return std::is_same<T, U>::value;
      }

      virtual std::vector<T>& List() override
      {
         return m_list;
      }

      virtual const std::vector<T>& ConstList() const override
      {
         return m_list;
      }

      /*
         ##################### ITERATORS #####################
      */

      /// @brief 							std begin iterator
      auto begin()
      {
         return m_list.begin();
      }

      /// @brief 							std end iterator
      auto end()
      {
         return m_list.end();
      }

      /// @brief 							std begin iterator
      auto cbegin() const
      {
         return m_list.begin();
      }

      /// @brief 							std end iterator
      auto cend() const
      {
         return m_list.end();
      }

      /// @brief 							std rbegin iterator
      auto rbegin()
      {
         return m_list.rbegin();
      }

      /// @brief 							std rend iterator
      auto rend()
      {
         return m_list.rend();
      }

      /// @brief 							std rbegin iterator
      auto crbegin() const
      {
         return m_list.rbegin();
      }

      /// @brief 							std rend iterator
      auto crend() const
      {
         return m_list.rend();
      }

      /// @brief 							get the elements as QVector
      QVector<T> ToQVector()
      {
         return QVector<T>::fromStdVector(m_list);
      }

      /*
         ##################### OPERATORS #####################
      */

      Linq<T>& operator<<(const T& other)
      {
         return Add(other);
      }
      Linq<T>& operator<<(T&& other)
      {
         return Add(std::move(other));
      }

      Linq<T>& operator<<(const Linq<T>& other)
      {
         return Merge(other);
      }
      Linq<T>& operator<<(Linq<T>&& other)
      {
         return Merge(std::move(other));
      }

      Linq<T>& operator<<(const std::vector<T>& other)
      {
         return Merge(std::move(Linq<T>(other)));
      }
      Linq<T>& operator<<(std::vector<T>&& other)
      {
         return Merge(std::move(Linq<T>(std::move(other))));
      }

      Linq<T>& operator<<(const std::initializer_list<T>& other)
      {
         return Merge(std::move(Linq<T>(other)));
      }
      Linq<T>& operator<<(std::initializer_list<T>&& other)
      {
         return Merge(std::move(Linq<T>(std::move(other))));
      }

      Linq<T>& operator>>(T& other)
      {
         other = First();
         return RemoveAt(0);
      }

      T& operator[](uint64_t ind)
      {
         return m_list[ind];
      }

      const T& operator[](uint64_t ind) const
      {
         return m_list[ind];
      }

      operator std::vector<T>() const
      {
         return m_list;
      }

      template<typename U>
      bool operator==(const BaseLinq<U>& other) const
      {
         static_assert(std::is_convertible<U, T>::value, "U is not convertible to T");
         return std::equal(m_list.begin(), m_list.end(), other.Get().begin(), other.Get().end());
      }

      template<typename U>
      bool operator==(const std::initializer_list<U>& other) const
      {
         static_assert(std::is_convertible<U, T>::value, "U is not convertible to T");
         return std::equal(m_list.begin(), m_list.end(), other.begin(), other.end());
      }

      template<typename U>
      bool operator==(const std::vector<U>& other) const
      {
         static_assert(std::is_convertible<U, T>::value, "U is not convertible to T");
         return std::equal(m_list.begin(), m_list.end(), other.begin(), other.end());
      }

      template<typename U>
      bool operator!=(const BaseLinq<U>& other) const
      {
         return !(operator==<U>(other));
      }

      template<typename U>
      bool operator!=(const std::initializer_list<U>& other) const
      {
         return !(operator==<U>(other));
      }

      template<typename U>
      bool operator!=(const std::vector<U>& other) const
      {
         return !(operator==<U>(other));
      }

      BaseLinq<T>& operator=(BaseLinq<T>&& other) = default;

      BaseLinq<T>& operator=(const BaseLinq<T>& other) = default;

      virtual ~BaseLinq() = default;

#pragma region C++17 Code

// determine cxx version on apple      
#if !defined _WIN32 && !defined _WIN64
#   if __cplusplus == 201703L
#       define HAS_CXX_17 1
#   endif
#endif

#if (_HAS_CXX17 == 1) || (HAS_CXX_17 == 1) // enabled only if Language is C++17 AND compiled with /Zc:__cplusplus (this is for visual studio to report the correct version number through macro)


      /// @brief 							helper struct to decide which function pointer to store
      template<bool same, typename FNC, typename ...DefaultArgs>
      struct SelectDecisionHelper {};
      /// @brief 							helper struct to decide which function pointer to store
      template<bool same, typename FNC, typename FNC2, typename ...DefaultArgs>
      struct SelectIfDecisionHelper {};
      /// @brief 							helper struct to decide which function pointer to store
      template<bool same, typename FNC, typename FNC2, typename ...DefaultArgs>
      struct IfSelectDecisionHelper {};

      template<typename FNC, typename ...DefaultArgs>
      struct SelectDecisionHelper<true, FNC, DefaultArgs...>
      { static inline auto value = &BaseLinq<T>::SelectInPlace<FNC, DefaultArgs...>; }; // <-- C++17 feature!

      template<typename FNC, typename ...DefaultArgs>
      struct SelectDecisionHelper<false, FNC, DefaultArgs...>
      { static inline auto value = &BaseLinq<T>::Select<FNC, DefaultArgs...>; }; // <-- C++17 feature!

      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      struct SelectIfDecisionHelper<true, FNC, FNC2, DefaultArgs...>
      { static inline auto value = &BaseLinq<T>::SelectInPlaceIf<FNC, FNC2, DefaultArgs...>; }; // <-- C++17 feature!

      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      struct SelectIfDecisionHelper<false, FNC, FNC2, DefaultArgs...>
      { static inline auto value = &BaseLinq<T>::SelectIf<FNC, FNC2, DefaultArgs...>; }; // <-- C++17 feature!

      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      struct IfSelectDecisionHelper<true, FNC, FNC2, DefaultArgs...>
      { static inline auto value = &BaseLinq<T>::IfSelectInPlace<FNC, FNC2, DefaultArgs...>; }; // <-- C++17 feature!

      template<typename FNC, typename FNC2, typename ...DefaultArgs>
      struct IfSelectDecisionHelper<false, FNC, FNC2, DefaultArgs...>
      { static inline auto value = &BaseLinq<T>::IfSelect<FNC, FNC2, DefaultArgs...>; }; // <-- C++17 feature!

      /// @brief 							executes function on every object of this BaseLinq and returns the BaseLinq
      /// @note  							whether or not this function copies or returns itself as reference
      /// @note  							depends on the return type of the function!
      /// @note  							if the result of \a pred is T, SelectInPlace will be executed (reference)
      /// @note  							else Select will be executed (copy)
      template<typename FNC, typename ...DefaultArgs>
      auto AutoSelect(FNC pred, DefaultArgs...args) ->
         typename std::conditional<
         !std::is_same<T, decltype(std::invoke(std::declval<FNC>(), *(T*) (0), std::declval<DefaultArgs>()...))>::value,
         decltype(std::invoke(SelectDecisionHelper<false, FNC, DefaultArgs...>::value, this, std::declval<FNC>(), std::declval<DefaultArgs>()...)),
         decltype(std::invoke(SelectDecisionHelper<std::is_same<T, decltype(std::invoke(std::declval<FNC>(), *(T*) (0), std::declval<DefaultArgs>()...))>::value, FNC, DefaultArgs...>::value, this, std::declval<FNC>(), std::declval<DefaultArgs>()...))>::type
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred, obj, args...);
         };

         using out_type = decltype(fnc(*(T*) (0)));
         constexpr bool ret_type_diff = !std::is_same<T, out_type>::value;

         // gather function pointers
         auto select_copy = SelectDecisionHelper<false, FNC, DefaultArgs...>::value;
         auto select_inplace = SelectDecisionHelper<!ret_type_diff, FNC, DefaultArgs...>::value;

         using copy_out_type = decltype(std::invoke(select_copy, this, pred, args...));
         using inplace_out_type = decltype(std::invoke(select_inplace, this, pred, args...));
         using expected_out_type = typename std::conditional<ret_type_diff, copy_out_type, inplace_out_type>::type;

         // decide which one to take as output type
         using method = typename std::conditional<ret_type_diff, decltype(select_copy), decltype(select_inplace)>::type;

         // create select-function pointer
         method* callable = nullptr;
         if (ret_type_diff)
            callable = static_cast<method*>(static_cast<void*>(&select_copy));
         else
            callable = static_cast<method*>(static_cast<void*>(&select_inplace));

         // call that function
         return std::invoke((*callable), this, pred, args...);
      }

      /// @brief 							executes function on every object of this BaseLinq and returns the BaseLinq
      /// @note  							whether or not this function copies or returns itself as reference
      /// @note  							depends on the return type of the function!
      /// @note  							if the result of \a pred is T, SelectInPlace will be executed (reference)
      /// @note  							else Select will be executed (copy)
      template<typename FNC1, typename FNC2, typename ...DefaultArgs>
      auto AutoSelectIf(FNC1 pred1, FNC2 pred2, DefaultArgs...args) ->
         typename std::conditional<
         !std::is_same<T, decltype(std::invoke(std::declval<FNC1>(), *(T*) (0), std::declval<DefaultArgs>()...))>::value,
         decltype(std::invoke(SelectIfDecisionHelper<false, FNC1, FNC2, DefaultArgs...>::value, this, std::declval<FNC1>(), std::declval<FNC2>(), std::declval<DefaultArgs>()...)),
         decltype(std::invoke(SelectIfDecisionHelper<std::is_same<T, decltype(std::invoke(std::declval<FNC1>(), *(T*) (0), std::declval<DefaultArgs>()...))>::value, FNC1, FNC2, DefaultArgs...>::value, this, std::declval<FNC1>(), std::declval<FNC2>(), std::declval<DefaultArgs>()...))>::type
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred1, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         auto sel = [&](out_type& obj)
         {
            return std::invoke(pred2, obj);
         };
         using sel_type = decltype(sel(*((out_type*) (0))));
         static_assert(std::is_convertible<sel_type, bool>::value, "the return type has to be a convertible to bool!");

         constexpr bool ret_type_diff = !std::is_same<T, out_type>::value;

         // gather function pointers
         auto select_copy = SelectIfDecisionHelper<false, FNC1, FNC2, DefaultArgs...>::value;
         auto select_inplace = SelectIfDecisionHelper<!ret_type_diff, FNC1, FNC2, DefaultArgs...>::value;

         // decide which one to take as output type
         using method = typename std::conditional<ret_type_diff, decltype(select_copy), decltype(select_inplace)>::type;

         // create select-function pointer
         method* callable = nullptr;
         if (ret_type_diff)
            callable = static_cast<method*>(static_cast<void*>(&select_copy));
         else
            callable = static_cast<method*>(static_cast<void*>(&select_inplace));

         // call that function
         return std::invoke((*callable), this, pred1, pred2, args...);
      }

      /// @brief 							executes function on every object of this BaseLinq and returns the BaseLinq
      /// @note  							whether or not this function copies or returns itself as reference
      /// @note  							depends on the return type of the function!
      /// @note  							if the result of \a pred is T, SelectInPlace will be executed (reference)
      /// @note  							else Select will be executed (copy)
      template<typename FNC1, typename FNC2, typename ...DefaultArgs>
      auto AutoIfSelect(FNC1 pred1, FNC2 pred2, DefaultArgs...args) ->
         typename std::conditional<
         !std::is_same<T, decltype(std::invoke(std::declval<FNC2>(), *(T*) (0), std::declval<DefaultArgs>()...))>::value,
         decltype(std::invoke(IfSelectDecisionHelper<false, FNC1, FNC2, DefaultArgs...>::value, this, std::declval<FNC1>(), std::declval<FNC2>(), std::declval<DefaultArgs>()...)),
         decltype(std::invoke(IfSelectDecisionHelper<std::is_same<T, decltype(std::invoke(std::declval<FNC2>(), *(T*) (0), std::declval<DefaultArgs>()...))>::value, FNC1, FNC2, DefaultArgs...>::value, this, std::declval<FNC1>(), std::declval<FNC2>(), std::declval<DefaultArgs>()...))>::type
      {
         auto fnc = [&](T& obj)
         {
            return std::invoke(pred2, obj, args...);
         };
         using out_type = decltype(fnc(*(T*) (0)));
         auto sel = [&](const T& obj)
         {
            return std::invoke(pred1, obj);
         };
         using sel_type = decltype(sel(*(T*) (0)));
         static_assert(std::is_convertible<sel_type, bool>::value, "the return type has to be a convertible to bool!");

         constexpr bool ret_type_diff = !std::is_same<T, out_type>::value;

         // gather function pointers
         auto select_copy = IfSelectDecisionHelper<false, FNC1, FNC2, DefaultArgs...>::value;
         auto select_inplace = IfSelectDecisionHelper<!ret_type_diff, FNC1, FNC2, DefaultArgs...>::value;

         // decide which one to take as output type
         using method = typename std::conditional<ret_type_diff, decltype(select_copy), decltype(select_inplace)>::type;

         // create select-function pointer
         method* callable = nullptr;
         if (ret_type_diff)
            callable = static_cast<method*>(static_cast<void*>(&select_copy));
         else
            callable = static_cast<method*>(static_cast<void*>(&select_inplace));

         // call that function
         return std::invoke((*callable), this, pred1, pred2, args...);
      }

#endif

#pragma endregion


   protected:

      static T OneToOne(size_type index)
      {
         return static_cast<T>(index);
      }

      std::vector<T> m_list;
   };
   template<typename T> struct remove_container<BaseLinq<T>> { using type = T; };
}
#endif
