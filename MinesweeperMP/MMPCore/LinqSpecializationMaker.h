//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef LinqSpecializationMaker_H
#define LinqSpecializationMaker_H

#include "LinqMacros.h"
#include "AbstractLinq.h"
#include "LinqStructs.h"
#include "BaseLinq.h"

// after creating your own specialization you need to add an include into "LinqSpecializations.h".
// If specialization for a type already exists with another specialization group you need to move
// the specialization declaration to "MixedLinqs.h" and re-implement all specialization groups again

// IMPORTANT!
// Implement a !VIRTUAL! destructor if you use LINQ_GROUP_END_CUSTOM_DESTRUCTOR(class_name)
// if you don't need a custom destructor use LINQ_GROUP_END(class_name)

// local (inherited) functions you can use:
//
// this->This()                 -> Linq<T>& with correct type
// this->ConstThis()            -> const version of This
// this->List()                 -> The current std::vector<T>& list (also available through 'This().Original()' )
// this->ConstList()            -> const version of List (also available through 'ConstThis().Get()' )

// on This() or ConstThis() you'll have all functions Linq<T> has so you can always use them
// note: do not reference your own functions 

#endif
