#include "Board.h"

namespace
{
	bool StateContains(int state, int contains)
	{
		return (state & contains) == contains;
	}
}

MMPCore::Mechanics::Game::Board::Board(int boardWidth, int boardHeight)
	: m_actualBoardWidth(boardWidth)
	, m_actualBoardHeight(boardHeight)
{
	std::fill(m_boardStates, m_boardStates + MaxBoardSize, CLOSED | NO_MINE);
	std::fill(m_boardStates, m_boardStates + GetActualBoardSize(), CLOSED | NO_MINE);
}

MMPCore::Mechanics::Game::Board::Board()
   : m_actualBoardWidth(8)
   , m_actualBoardHeight(16)
{
   std::fill(m_boardStates, m_boardStates + MaxBoardSize, CLOSED | NO_MINE);
   std::fill(m_boardStates, m_boardStates + GetActualBoardSize(), CLOSED | NO_MINE);
}

MMPCore::Mechanics::Game::Board::~Board()
{}

MMPCore::Mechanics::Game::Board::FieldState MMPCore::Mechanics::Game::Board::GetFieldState(std::size_t x, std::size_t y) const
{
	return static_cast<FieldState>(m_boardStates[y * m_actualBoardWidth + x]);
}

void MMPCore::Mechanics::Game::Board::SetFieldState(std::size_t x, std::size_t y, FieldState state)
{
	m_boardStates[y * m_actualBoardWidth + x] = static_cast<uchar>(state);
}

std::size_t MMPCore::Mechanics::Game::Board::GetBoardWidth() const
{
	return m_actualBoardWidth;
}

std::size_t MMPCore::Mechanics::Game::Board::GetBoardHeight() const
{
	return m_actualBoardHeight;
}

void MMPCore::Mechanics::Game::Board::SetBoardSize(std::size_t w, std::size_t h)
{
   m_actualBoardWidth = w;
   m_actualBoardHeight = h;

	std::fill(m_boardStates, m_boardStates + MaxBoardSize, CLOSED | NO_MINE);
	std::fill(m_boardStates, m_boardStates + GetActualBoardSize(), CLOSED | NO_MINE);
}

int MMPCore::Mechanics::Game::Board::CountMines(std::size_t x, std::size_t y) const
{
	int mines = 0;

	for (int _x = -1; _x <= 1; _x++)
	{
		for (int _y = -1; _y <= 1; _y++)
		{
			auto tx = _x + x;
			auto ty = _y + y;

			if (tx >= 0 && ty >= 0 &&
				 tx < m_actualBoardWidth && ty < m_actualBoardHeight)
			{
				if (FieldStateContains(tx, ty, HAS_MINE))
				{
					mines++;
				}
			}
		}
	}

	return mines;
}

bool MMPCore::Mechanics::Game::Board::FieldStateContains(std::size_t x, std::size_t y, FieldState state) const
{
	return StateContains(m_boardStates[y * m_actualBoardWidth + x], state);
}

const uchar* MMPCore::Mechanics::Game::Board::cbegin() const
{
	return m_boardStates;
}

const uchar* MMPCore::Mechanics::Game::Board::cend() const
{
	return m_boardStates + GetActualBoardSize();
}

uchar* MMPCore::Mechanics::Game::Board::end()
{
	return m_boardStates;
}

uchar* MMPCore::Mechanics::Game::Board::begin()
{
	return m_boardStates + GetActualBoardSize();
}

void MMPCore::Mechanics::Game::Board::SetFieldState(std::size_t x, std::size_t y, int state)
{
	m_boardStates[y * m_actualBoardWidth + x] = static_cast<uchar>(state);
}

std::size_t MMPCore::Mechanics::Game::Board::GetActualBoardSize() const
{
	return m_actualBoardWidth * m_actualBoardHeight;
}
