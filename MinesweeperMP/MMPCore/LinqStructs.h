//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#ifndef LinqStructs_H
#define LinqStructs_H

#include "AbstractLinq.h"
#include "LinqMacros.h"

#include <QVector>
#include <QStringList>
#include <QString>

#include <vector>

#include <functional>

namespace Common
{
	/*
		##################### remove_container #####################
	*/
	template<typename T> struct remove_container { using type = T; };

	template<typename T> struct remove_container<std::vector<T>> { using type = T; };
	template<typename T> struct remove_container<QVector<T>> { using type = T; };
	template<typename T> struct remove_container<QList<T>> { using type = T; };
	template<typename T> struct remove_container<std::initializer_list<T>> { using type = T; };
	template<> struct remove_container<QStringList> { using type = QString; };

	/*
		##################### is_one_of #####################
	*/

	/*template<typename T, typename First, typename ...Rest> struct is_one_of
	{ enum { value = std::is_same<T, First>::value || is_one_of<T, Rest...>::value }; };
	template<typename T, typename U> struct is_one_of<T, U>
	{ enum { value = std::is_same<T, U>::value }; };*/

	/*
		##################### General abstract Linq #####################
    */

    template<typename T> struct BaseLinq;

#pragma warning(push)
#pragma warning(disable : 4584)
	template<typename T> struct Linq _LINQ_MULTI_SPECIALIZATION(T, public LinqData<T>);
#pragma warning(pop)

	/*
		##################### ToLinq #####################
	*/

	template<typename T, typename U>
	Linq<T>& ToLinq(U* obj)
	{
		return *dynamic_cast<Linq<T>*>(obj);
	}

	template<typename T, typename U>
	const Linq<T>& ToConstLinq(const U* obj)
	{
		return *dynamic_cast<const Linq<T>*>(obj);
	}

	/*
		##################### Function Conversion #####################
	*/

	template<typename FNC>
	static inline auto Negate(FNC fnc)
	{
		static auto __func = [fnc](const auto& a) { return (!(fnc(a))); };
		return __func;
	}

	static inline auto Self()
	{
		static auto __func = [](const auto& a) { return a; };
		return __func;
	}

	static inline auto IsNull()
	{
		static auto __func = [](const auto& a) -> bool { return ((a) == 0); };
		return __func;
	}

	static inline auto NotNull()
	{
		static auto __func = [](const auto& a) -> bool { return ((a) != 0); };
		return __func;
	}

	/*
		##################### Function Binding #####################
	*/

	template<int ARGS = 0>
	struct Binder
	{
		template<typename FNC, typename T>
		static auto Bind(FNC fnc, T* obj)
		{
			static auto __func = [fnc, obj]() { return std::invoke(fnc, obj); };
			return __func;
		}
	};

	LINQ_BINDER(1, (const auto& a), a);
	LINQ_BINDER(2, (const auto& a, const auto& b), a, b);
	LINQ_BINDER(3, (const auto& a, const auto& b, const auto& c), a, b, c);
	LINQ_BINDER(4, (const auto& a, const auto& b, const auto& c, const auto& d), a, b, c, d);
	LINQ_BINDER(5, (const auto& a, const auto& b, const auto& c, const auto& d, const auto& e), a, b, c, d, e);
	LINQ_BINDER(6, (const auto& a, const auto& b, const auto& c, const auto& d, const auto& e, const auto& f), a, b, c, d, e, f);
	LINQ_BINDER(7, (const auto& a, const auto& b, const auto& c, const auto& d, const auto& e, const auto& f, const auto& g), a, b, c, d, e, f, g);
	LINQ_BINDER(8, (const auto& a, const auto& b, const auto& c, const auto& d, const auto& e, const auto& f, const auto& g, const auto& h), a, b, c, d, e, f, g, h);
	LINQ_BINDER(9, (const auto& a, const auto& b, const auto& c, const auto& d, const auto& e, const auto& f, const auto& g, const auto& h, const auto& i), a, b, c, d, e, f, g, h, i);
	LINQ_BINDER(10, (const auto& a, const auto& b, const auto& c, const auto& d, const auto& e, const auto& f, const auto& g, const auto& h, const auto& i, const auto& j), a, b, c, d, e, f, g, h, i, j);

	template<typename R, typename CLS, typename ...ARGS>
	constexpr std::integral_constant<unsigned, sizeof...(ARGS)> argumentSize(R(CLS::*fnc)(ARGS...))
	{
		return std::integral_constant<unsigned, sizeof...(ARGS)>{};
	}

	template<int ARGS, typename FNC, typename T>
	static inline auto Bind(FNC fnc, T obj)
	{
		return Binder<ARGS>::Bind(fnc, obj);
	}

	template<typename FNC, typename T>
	static inline auto Bind(FNC fnc, T obj)
	{
		return Binder<decltype(argumentSize(fnc))::value>::Bind(fnc, obj);
	}
}

#endif
