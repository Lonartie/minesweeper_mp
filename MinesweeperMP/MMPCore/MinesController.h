#pragma once

#include "stdafx.h"
#include "Serializable.h"
#include "Board.h"
#include "Linq.h"

namespace MMPCore
{
	namespace Mechanics
	{
		namespace Game
		{
			class MMPCORE_EXPORT MinesController: public MMPCore::Serialization::Serializable
			{
            SERIALIZABLE_CLASS;

			public:

				enum PlayMode
				{
					Easy,
					Medium,
					Hard,
					Felix,
				};

				Common::Linq<int> percentages = 
				{
					5,
					15,
					20,
					28
				};

				MinesController();
				MinesController(PlayMode mode);
				~MinesController();

				void InitBoard(Board& board);

			private:

				SERIALIZABLE(PlayMode, m_playMode) = Easy;

			};
		}
	}
}
