//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef LinqSpecializations_H
#define LinqSpecializations_H

#include "CharLinq.h"
#include "NumericLinq.h"
#include "QStringLinq.h"
#include "OldStringLinq.h"

#include "MixedLinqs.h"

#endif
