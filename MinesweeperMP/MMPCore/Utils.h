#pragma once

#include "stdafx.h"

namespace MMPCore
{
   namespace Utils
   {
      std::size_t RandomGUID();
   }

}

