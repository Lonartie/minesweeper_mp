#pragma once

#include "stdafx.h"

namespace MMPCore
{
   namespace Networking
   {
      namespace CommonRequests
      {
         namespace
         {
            inline QByteArray Raw(const QString& str)
            {
               return str.toLatin1();
            }
         }

         static const QByteArray 
            AuthenticationRequest = Raw("AUTH_YOURSELF"),
            AuthenticationSuccessfull = Raw("AUTH_SUCCESS"),
            AuthenticationFailed = Raw("AUTH_FAIL");
      }
   }
}