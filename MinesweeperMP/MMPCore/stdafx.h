/// Project configuration

#ifdef PROJECT_NAME
#	undef PROJECT_NAME
#endif

#ifdef USE_GUI
#	undef USE_GUI
#endif

#define PROJECT_NAME MMPCore
#define USE_GUI false

/// includes

#if USE_GUI == false
#  include "dll.h"
#endif

// Qt
#include <QtCore>
#include <QtNetwork>
#include <QtWebSockets>

#if USE_GUI == true
#	include <QtGui>
#	include <QtWidgets>
#endif

// stl
#include <vector>
#include <memory>
#include <functional>
#include <cmath>
#include <cassert>

// macros

#define MEMORY(class_name) \
public: \
	using SPtr = std::shared_ptr<class_name>; \
	using UPtr = std::unique_ptr<class_name>; \
	template<typename ...Args> SPtr CreateShared(Args...args) { return std::make_shared<class_name>(args...); } \
	template<typename ...Args> UPtr CreateShared(Args...args) { return std::make_unique<class_name>(args...); } \
private:



#define REQUIRES(cond, ...) if(!( cond )) return __VA_ARGS__ ;