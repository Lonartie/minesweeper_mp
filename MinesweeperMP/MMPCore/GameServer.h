#pragma once

#include "stdafx.h"
#include "Server.h"


namespace MMPCore
{
   namespace Networking
   {
      class GameServer: public Server
      {
         Q_OBJECT

      public:

         GameServer(QObject* parent = Q_NULLPTR);
         ~GameServer() = default;

      private:


      };

   }
}