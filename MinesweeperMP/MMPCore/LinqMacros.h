//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#ifndef LinqMacros_H
#define LinqMacros_H

#define FUNCTION(...) [&] (__VA_ARGS__)

#define MEMBER_FUNCTION(name, ...) FUNCTION(auto& x) { return x.name(__VA_ARGS__); }
#define MEMBER_FUNCTION_PTR(name, ...) FUNCTION(auto& x) { return x->name(__VA_ARGS__); }

/// @brief creates a lambda function like so
/// @note Lambda1(a, b) = [&](auto& a){ return (b) ; }
/// @note Lambda2(a, b, c) = [&](auto& a, auto& b){ return (c) ; }
/// @note etc...
/// <h3>Example</h3>
/// @code 
/// int input = 3;
/// int result = Lambda2(a, b, a + b) (input, input);
/// "result has the value: 6"
/// @endcode
#define Lambda1(a, func) FUNCTION(auto &a) { return (func) ; }
#define Lambda2(a, b, func) FUNCTION(auto &a, auto &b) { return (func) ; }
#define Lambda3(a, b, c, func) FUNCTION(auto &a, auto &b, auto &c) { return (func) ; }
#define Lambda4(a, b, c, d, func) FUNCTION(auto &a, auto &b, auto &c, auto &d) { return (func) ; }
#define Lambda5(a, b, c, d, e, func) FUNCTION(auto &a, auto &b, auto &c, auto &d, auto& e) { return (func) ; }
#define Lambda6(a, b, c, d, e, f, func) FUNCTION(auto &a, auto &b, auto &c, auto &d, auto& e, auto& f) { return (func) ; }
#define Lambda7(a, b, c, d, e, f, g, func) FUNCTION(auto &a, auto &b, auto &c, auto &d, auto& e, auto& f, auto& g) { return (func) ; }
#define Lambda8(a, b, c, d, e, f, g, h, func) FUNCTION(auto &a, auto &b, auto &c, auto &d, auto& e, auto& f, auto& g, auto& h) { return (func) ; }
#define Lambda9(a, b, c, d, e, f, g, h, i, func) FUNCTION(auto &a, auto &b, auto &c, auto &d, auto& e, auto& f, auto& g, auto& h, auto& i) { return (func) ; }
#define Lambda10(a, b, c, d, e, f, g, h, i, j, func) FUNCTION(auto &a, auto &b, auto &c, auto &d, auto& e, auto& f, auto& g, auto& h, auto& i, auto& j) { return (func) ; }
#define Lambda(x) Lambda##x

#define GET_ARG_COUNT(...)  INTERNAL_EXPAND_ARGS_PRIVATE(INTERNAL_ARGS_AUGMENTER(__VA_ARGS__))

#define INTERNAL_ARGS_AUGMENTER(...) unused, __VA_ARGS__
#define INTERNAL_EXPAND(x) x
#define INTERNAL_EXPAND_ARGS_PRIVATE(...) INTERNAL_EXPAND(INTERNAL_GET_ARG_COUNT_PRIVATE(__VA_ARGS__, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0))
#define INTERNAL_GET_ARG_COUNT_PRIVATE(_1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_, _11_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_, _20_, _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_, _31_, _32_, _33_, _34_, _35_, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _70, count, ...) count

#define COUNT(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value
#define _CAT(a, b) a##b
#define CAT(a, b) _CAT(a, b)
#define _INDIRECT(...) __VA_ARGS__
#define INDIRECT(...) _INDIRECT(__VA_ARGS__)
#define _INDIRECT_CALL(FUNC, ...) FUNC(__VA_ARGS__)
#define INDIRECT_CALL(FUNC, ...) _INDIRECT_CALL(FUNC, __VA_ARGS__)
#define FIRST(a, ...) a
#define REST(a, ...) __VA_ARGS__
#define GET_CORRECT_LAMBDA(...) INDIRECT_CALL(Lambda,GET_ARG_COUNT(REST(__VA_ARGS__)))
#define CALL_REST(CALLER, ...) CALLER, __VA_ARGS__)
#define INDIRECT_EXPAND(m, args) m args

/// @brief shortcut to Lambda1, Lambda2.. etc
/// @note it will act like following:
/// @note lambda(a, b) = Lambda1(a, b)
/// @note lambda(a, b, c) = Lambda2(a, b, c)
/// @note lambda(a, b, c, d) = Lambda3(a, b, c, d)
/// @note etc..
#define lambda(...) INDIRECT_EXPAND(GET_CORRECT_LAMBDA(__VA_ARGS__),(__VA_ARGS__))

#define Linq_Lambda_Body(func) { return (func) ; }
#define Linq_Lambda(...) FUNCTION(__VA_ARGS__) Linq_Lambda_Body

/// @brief shortcut to Lambda1, Lambda2.. etc
/// @note it will act like following:
/// @note lambda(a, b) = Lambda1(a, b)
/// @note lambda(a, b, c) = Lambda2(a, b, c)
/// @note lambda(a, b, c, d) = Lambda3(a, b, c, d)
/// @note etc..
#define L(...) lambda(__VA_ARGS__)
#define LAMBDA(args, func) FUNCTION args { return (func); }

#define LINQ_GROUP_BEGIN(class_name) template<typename T> struct class_name : public LinqData<T> {
#define LINQ_GROUP_END(class_name) public: virtual ~ class_name () {} }; template<typename T> struct remove_container<class_name<T>> { using type = T; };
#define LINQ_GROUP_END_CUSTOM_DESTRUCTOR(class_name) }; template<typename T> struct remove_container<class_name<T>> { using type = T; };


#define _LINQ_MULTI_SPECIALIZATION(type, ...)                                                                  \
 final : public BaseLinq<type>, __VA_ARGS__                                                                    \
{                                                                                                              \
   using BaseLinq<type>::BaseLinq;                                                                             \
   Linq() : BaseLinq<type>() { }                                                                               \
   Linq(const Linq<type>& list) : BaseLinq<type>(list) {}                                                      \
   Linq(Linq<type>&& list) noexcept : BaseLinq<type>(std::move(list)) {}                                       \
   virtual std::vector<type>& List() override { return BaseLinq<type>::List(); }                               \
   virtual const std::vector<type>& ConstList() const override { return BaseLinq<type>::ConstList(); }         \
   virtual Linq<type>& This() override { return *this; }                                                       \
   virtual const Linq<type>& ConstThis() const override { return *this; }                                      \
   /*SetTo - Operators*/                                                                                       \
   Linq<type>& operator=(const Linq<type>& list) noexcept { this->m_list = list.ConstList(); return *this; }   \
   Linq<type>& operator=(Linq<type>&& list) noexcept { this->m_list = std::move(list.List()); return *this; }  \
   Linq<type>& operator=(const std::vector<type>& list) noexcept { this->m_list = list; return *this;}         \
   Linq<type>& operator=(std::vector<type>&& list) noexcept { this->m_list = std::move(list); return *this; }  \
   Linq<type>& operator=(const std::initializer_list<type>& list) noexcept                                     \
   { this->m_list = std::vector<type>(list); return *this; }                                                   \
   Linq<type>& operator=(std::initializer_list<type>&& list) noexcept                                          \
   { this->m_list = std::move(std::vector<type>(std::move(list))); return *this; }                             \
                                                                                                               \
   virtual ~Linq() = default;                                                                                  \
};

#define LINQ_MULTI_SPECIALIZATION(type, ...)                                                                   \
template<> struct Linq<type> _LINQ_MULTI_SPECIALIZATION(type, __VA_ARGS__)

#define LINQ_SPECIALIZATION(type, group) LINQ_MULTI_SPECIALIZATION(type, public group<type>)

#define LINQ_BINDER(num, args, ...)																							\
template<>																															\
struct Binder<num>																												\
{																																		\
	template<typename FNC, typename T>																						\
	static auto Bind(FNC fnc, T* obj)																						\
	{																																	\
		static auto __func = [fnc, obj] args { return std::invoke(fnc, obj, __VA_ARGS__ ); };				\
		return __func;																												\
	}																																	\
};

#endif
