#pragma once

#include "stdafx.h"
#include "Serializable.h"
#include "Friend.h"

namespace MMPCore
{
   namespace Identity
   {
      class MMPCORE_EXPORT FriendList: public MMPCore::Serialization::Serializable
      {
         Q_OBJECT;
         SERIALIZABLE_CLASS;

         static constexpr std::size_t MaxFriends = 255;

      public:
         FriendList();
         FriendList(const FriendList& fl) noexcept;
         ~FriendList();

         void AddFriend(const MMPCore::Identity::Friend& fr);
         void RemoveFriend(const MMPCore::Identity::Friend& fr);
         Common::Linq<MMPCore::Identity::Friend> GetFriends() const;
         bool ContainsFriend(const MMPCore::Identity::Friend& fr) const;

      private:

         void CleanMemory();

         SERIALIZABLE_ARRAY(MMPCore::Identity::Friend, m_friends, MaxFriends);
         SERIALIZABLE(std::size_t, m_numberOfFriends) = 0;

      };

   }
}