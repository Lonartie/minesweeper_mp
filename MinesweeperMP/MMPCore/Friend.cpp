#include "Friend.h"

MMPCore::Identity::Friend::Friend()
{

}

MMPCore::Identity::Friend::Friend(const Friend& fr)
{
   name = fr.name;
}

MMPCore::Identity::Friend::~Friend()
{

}

MMPCore::Identity::Friend& MMPCore::Identity::Friend::operator=(Friend&& fr) noexcept
{
   name = std::move(fr.name);
   return *this;
}

MMPCore::Identity::Friend& MMPCore::Identity::Friend::operator=(const Friend& fr) noexcept
{
   name = fr.name;
   return *this;
}

bool MMPCore::Identity::Friend::operator!=(const Friend& fr) const noexcept
{
   return !operator==(fr);
}

bool MMPCore::Identity::Friend::operator==(const Friend& fr) const noexcept
{
   return fr.name == name;
}
