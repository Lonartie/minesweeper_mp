#pragma once

#include "stdafx.h"
#include "Serializable.h"
#include "Player.h"
#include "Board.h"

namespace MMPCore
{
   namespace Mechanics
   {
      namespace Game
      {
         class MMPCORE_EXPORT Flags : public MMPCore::Serialization::Serializable
         {
            SERIALIZABLE_CLASS;
            MEMORY(Flags);

         public /*static*/:
            
            static constexpr std::size_t MaxFlagsWidth = MMPCore::Mechanics::Game::Board::MaxBoardWidth;
            static constexpr std::size_t MaxFlagsHeight = MMPCore::Mechanics::Game::Board::MaxBoardHeight;
            static constexpr std::size_t MaxFlags = MaxFlagsWidth * MaxFlagsHeight;

         public /*construction*/:

            Flags(const MMPCore::Identity::Player& host);
            Flags() = default;
            ~Flags() = default;

         public /*methods*/:

            void SetHost(const MMPCore::Identity::Player& host);
         
         private /*members*/:

            SERIALIZABLE(std::size_t, m_hostID) = 0;
            SERIALIZABLE_ARRAY(bool, m_flags, MaxFlags);

         };
      }
   }
}