#pragma once

#include "stdafx.h"
#include "Serializable.h"

namespace MMPCore
{
	namespace Identity
	{
		class MMPCORE_EXPORT Player : public MMPCore::Serialization::Serializable
		{
			MEMORY(Player);
         SERIALIZABLE_CLASS;

		public:
			static constexpr std::size_t NAME_SIZE = 255;

			Player(const QString& name);
			Player(const Player& player);
			Player();
			virtual ~Player();

			QString GetName() const;
			std::size_t GetID() const;
			bool IsValid() const;

			Player& operator=(const Player& player);
			Player& operator=(Player&& player) noexcept;
         bool operator==(const Player& player) const noexcept;

		private:

			SERIALIZABLE_ARRAY(char, m_name, NAME_SIZE);
			SERIALIZABLE(std::size_t, m_ID) = 0;
			SERIALIZABLE(bool, m_valid) = false;
		};
	}
}