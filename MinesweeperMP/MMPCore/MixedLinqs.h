//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef MixedLinqs_H
#define MixedLinqs_H

#include "NumericLinq.h"
#include "CharLinq.h"

namespace Common
{
   /*
      ##################### NumericLinq AND CharLinq #####################
   */

   LINQ_MULTI_SPECIALIZATION(char,
                             public NumericLinq<char>,
                             public CharLinq<char>);
}

#endif
