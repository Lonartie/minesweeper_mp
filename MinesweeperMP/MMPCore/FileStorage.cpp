#include "FileStorage.h"

MMPCore::Mechanics::Storage::FileStorage::FileStorage()
{

}

MMPCore::Mechanics::Storage::FileStorage::~FileStorage()
{

}

void MMPCore::Mechanics::Storage::FileStorage::SetOverrideMode(bool mode /*= false*/)
{
   m_override = mode;
}

bool MMPCore::Mechanics::Storage::FileStorage::RecoverToMemory(const QString& path)
{
   if (!KeyExistsOnDrive(path)) return false;
   if (KeyExistsInMemory(path)) return true;
   
   return SaveInMemory(path, LoadDataFromFile(QFileInfo(path).absoluteFilePath()));
}

bool MMPCore::Mechanics::Storage::FileStorage::Save(const QString& key, const QByteArray& data)
{
   return SaveInMemory(key, data) && SaveOnDrive(key, data);
}

void MMPCore::Mechanics::Storage::FileStorage::Remove(const QString& key)
{
   RemoveBoth(key);
}

QByteArray MMPCore::Mechanics::Storage::FileStorage::Load(const QString& key) const
{
   if (!KeyExists(key)) return QByteArray();
   return LoadDataFromFile(GetPathOfKey(key));
}

Common::Linq<QString> MMPCore::Mechanics::Storage::FileStorage::GetKeys() const
{
   return m_data.
      Select(&std::pair<QString, QString>::first);
}

void MMPCore::Mechanics::Storage::FileStorage::Clear() noexcept
{
   for (auto& key : GetKeys())
   {
      RemoveBoth(key);
   }
}

bool MMPCore::Mechanics::Storage::FileStorage::SaveInMemory(const QString& key, const QByteArray& data)
{
   if (KeyExistsInMemory(key)) return false;

   m_data.Add({key, QFileInfo(key).absoluteFilePath()});
   return true;
}

bool MMPCore::Mechanics::Storage::FileStorage::SaveOnDrive(const QString& key, const QByteArray& data)
{
   if (KeyExistsOnDrive(key) && !m_override) return false;

   QFile file(key);
   if (!file.open(QIODevice::WriteOnly)) return false;
   file.write(data);
   file.flush();
   file.close();
   return true;
}

bool MMPCore::Mechanics::Storage::FileStorage::KeyExistsOnDrive(const QString& key) const
{
   return QFileInfo(key).exists();
}

bool MMPCore::Mechanics::Storage::FileStorage::KeyExistsInMemory(const QString& key) const
{
   return m_data.
      Where(lambda(x, x.first.compare(key, Qt::CaseInsensitive))).
      Any();
}

bool MMPCore::Mechanics::Storage::FileStorage::KeyExists(const QString& key) const
{
   return KeyExistsInMemory(key) || KeyExistsOnDrive(key);
}

bool MMPCore::Mechanics::Storage::FileStorage::RemoveInMemory(const QString& key)
{
   if (!KeyExistsInMemory(key)) return false;

   m_data.
      Remove(m_data.
             Where(lambda(x, x.first == key)).
             First());
   return true;
}

bool MMPCore::Mechanics::Storage::FileStorage::RemoveOnDrive(const QString& key)
{
   if (!KeyExistsOnDrive(key)) return false;
   return QFile(key).remove();
}

bool MMPCore::Mechanics::Storage::FileStorage::RemoveBoth(const QString& key)
{
   return RemoveInMemory(key) && RemoveOnDrive(key);
}

QByteArray MMPCore::Mechanics::Storage::FileStorage::LoadDataFromFile(const QString& path) const
{
   QFile file(path);
   if (!file.exists()) return QByteArray();
   if (!file.open(QIODevice::ReadOnly)) return QByteArray();
   auto data = file.readAll();
   file.close();
   return data;
}

QString MMPCore::Mechanics::Storage::FileStorage::GetPathOfKey(const QString& key) const
{
   return QFileInfo(m_data.
                    Where(lambda(x, x.first == key)).
                    FirstOrDefault().
                    second).absoluteFilePath();
}
