#include "GameRulesController.h"

MMPCore::Mechanics::Game::GameRulesController::GameRulesController(const MMPCore::Identity::Player& initiator,
                                                                   const Common::Linq<MMPCore::Identity::Player>& contestants,
                                                                   QObject* parent)
   : m_initiator(initiator)
{
   auto _contestants(contestants);
   if (!contestants.Contains(initiator)) _contestants.AddAt(0, initiator);

   m_numberOfContestants = std::min(_contestants.size(), MaxNumberOfContestants);
   for (int i = 0; i < m_numberOfContestants; i++)
   {
      m_contestants[i] = _contestants[i];
   }

   m_currentPlayerID = _contestants[0].GetID();
   m_nextPlayerID = _contestants[1 % m_numberOfContestants].GetID();
   m_lastPlayerID = _contestants[m_numberOfContestants - 1].GetID();
}

MMPCore::Mechanics::Game::GameRulesController::~GameRulesController()
{}

bool MMPCore::Mechanics::Game::GameRulesController::MoveAllowed(std::size_t x, std::size_t y,
                                                                const MMPCore::Identity::Player& player,
                                                                const MMPCore::Mechanics::Game::Board& board)
{
   // needs to be current player
   if (!player.GetID() == m_currentPlayerID)
      return false;

   // x needs to be inside the board
   if (x < 0 || x >= board.GetBoardWidth())
      return false;

   // y needs to be inside the board
   if (y < 0 || y >= board.GetBoardHeight())
      return false;

   // the field cannot already be opened
   if (board.FieldStateContains(x, y, Board::OPEN))
      return false;

   return true;
}

MMPCore::Identity::Player MMPCore::Mechanics::Game::GameRulesController::GetCurrentPlayer() const
{
   return GetContestants().Where([this](auto& cont) { return cont.GetID() == m_currentPlayerID; }).FirstOrDefault();
}

MMPCore::Identity::Player MMPCore::Mechanics::Game::GameRulesController::GetLastPlayer() const
{
   return GetContestants().Where([this](auto& cont) { return cont.GetID() == m_lastPlayerID; }).FirstOrDefault();
}

MMPCore::Identity::Player MMPCore::Mechanics::Game::GameRulesController::GetNextPlayer() const
{
   return GetContestants().Where([this](auto& cont) { return cont.GetID() == m_nextPlayerID; }).FirstOrDefault();
}

void MMPCore::Mechanics::Game::GameRulesController::PlayerMadeMove()
{
   m_lastPlayerID = m_currentPlayerID;
   m_currentPlayerID = m_nextPlayerID;

   auto index_current = GetContestants().Select(&MMPCore::Identity::Player::GetID).IndexOf(m_currentPlayerID);
   auto index_next = (index_current + 1) % m_numberOfContestants;
   m_nextPlayerID = GetContestants()[index_next].GetID();
}

const MMPCore::Identity::Player& MMPCore::Mechanics::Game::GameRulesController::GetInitiator() const
{
   return m_initiator;
}

Common::Linq<MMPCore::Identity::Player> MMPCore::Mechanics::Game::GameRulesController::GetContestants() const
{
   return Common::CreateLinq(std::vector<MMPCore::Identity::Player>(m_contestants, m_contestants + m_numberOfContestants));
}
