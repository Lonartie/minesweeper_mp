// MMPCore
#include "Player.h"

// stl
#include <time.h>

namespace
{
	char* ToQCharArray(const QString& name, std::size_t size)
	{
      char* str = new char[name.size()];

      int i = 0;
      for (char ch : name.toStdString())
      {
         str[i++] = ch;
      }

      return str;
	}

	QString FromQCharArray(const char* const data, std::size_t size)
	{
      return QString(QByteArray(data, size));
	}
}

MMPCore::Identity::Player::Player(const QString& name)
{
   std::fill(m_name, m_name + NAME_SIZE, '\0');
	auto arr = ToQCharArray(name, NAME_SIZE);
	std::memcpy(m_name, arr, name.size());
	delete[] arr;
	m_ID = rand() % time(0);
	m_valid = true;
}

MMPCore::Identity::Player::Player()
{
   std::fill(m_name, m_name + NAME_SIZE, '\0');
}

MMPCore::Identity::Player::Player(const Player& player)
{
   std::fill(m_name, m_name + NAME_SIZE, '\0');
	std::memcpy(m_name, player.m_name, player.GetName().size());
	m_ID = player.m_ID;
	m_valid = player.m_valid;
}

MMPCore::Identity::Player::~Player()
{}

MMPCore::Identity::Player& MMPCore::Identity::Player::operator=(const Player& player)
{
	std::memcpy(m_name, player.m_name, NAME_SIZE);
	m_ID = player.m_ID;
	m_valid = player.m_valid;
	return *this;
}

MMPCore::Identity::Player& MMPCore::Identity::Player::operator=(Player&& player) noexcept
{
	std::memmove(m_name, player.m_name, player.GetName().size());
	m_ID = std::move(player.m_ID);
	m_valid = std::move(player.m_valid);
	return *this;
}

QString MMPCore::Identity::Player::GetName() const
{
	return FromQCharArray(m_name, NAME_SIZE);
}

std::size_t MMPCore::Identity::Player::GetID() const
{
	return m_ID;
}

bool MMPCore::Identity::Player::IsValid() const
{
	return m_valid;
}

bool MMPCore::Identity::Player::operator==(const Player& player) const noexcept
{
	bool same = true;
	same = same && (player.m_ID == m_ID);
	same = same && (player.m_valid == m_valid);

	for (int i = 0; i < NAME_SIZE; i++)
		same = same && (player.m_name[i] == m_name[i]);

	return same;
}
