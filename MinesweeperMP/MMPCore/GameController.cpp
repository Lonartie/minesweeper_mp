#include "GameController.h"

using namespace Common;
using namespace MMPCore::Identity;
using namespace MMPCore::Networking;

/*
	#################### PUBLIC ####################
*/

MMPCore::Mechanics::Game::GameController::GameController(QObject* parent /*= Q_NULLPTR*/)
   : m_server(2555)
{
   ConnectSignals();
}

MMPCore::Mechanics::Game::GameController::~GameController()
{

}

/*
	#################### PRIVATE ####################
*/

void MMPCore::Mechanics::Game::GameController::ConnectSignals()
{
   //connect(&m_server, &Server::PlayerTriesToConnect, this, &GameController::AcceptOrDenyPlayerConnection);
}

Linq<ReceiverMethod> MMPCore::Mechanics::Game::GameController::CreateReceiverMethods()
{
   return CreateLinq<ReceiverMethod>(
      {
         CreateReceiver<Player>(Bind(&GameController::PlayerDataSent, this)),
         CreateReceiver<Friend>(Bind(&GameController::FriendDataSent, this)),
         CreateReceiver<FriendList>(Bind(&GameController::FriendListDataSent, this)),
      }
   );
}

/*
	#################### PRIVATE SLOTS ####################
*/

void MMPCore::Mechanics::Game::GameController::AcceptOrDenyPlayerConnection(MMPCore::Identity::Friend fr)
{
   /*if (m_friendList.ContainsFriend(fr))
      m_server.*/
}

/*
	#################### ReceiverMethods ####################
*/

void MMPCore::Mechanics::Game::GameController::PlayerDataSent(const Friend& sender, Player* player)
{

}

void MMPCore::Mechanics::Game::GameController::FriendDataSent(const MMPCore::Identity::Friend& sender, MMPCore::Identity::Friend* fr)
{

}

void MMPCore::Mechanics::Game::GameController::FriendListDataSent(const MMPCore::Identity::Friend& sender, MMPCore::Identity::FriendList* fr)
{

}


