#include "Server.h"
#include "CommonRequests.h"

using namespace MMPCore::Identity;

MMPCore::Networking::Server::Server(unsigned port, QObject* parent /*= Q_NULLPTR*/) : QObject(parent)
   , server(new QTcpServer(this))
{
   if (!server->listen(QHostAddress::Any, static_cast<quint16>(port)))
   {
      throw std::exception("server could not start");
   }

   m_updateTimer.start();
   connect(&m_updateTimer, SIGNAL(timeout()), this, SLOT(UpdateTimerTick()));
   connect(server, SIGNAL(newConnection()), this, SLOT(NewConnection()));
}

MMPCore::Networking::Server::~Server()
{
   m_updateTimer.stop();
}

void MMPCore::Networking::Server::BroadcastData(const QByteArray& data)
{
   m_clients.Foreach(Common::Bind(&Server::SendData, this), data);
}

void MMPCore::Networking::Server::SendData(QTcpSocket* socket, const QByteArray& data)
{
   socket->write(data);
   socket->waitForBytesWritten();
}

void MMPCore::Networking::Server::NewConnection()
{
   auto socket = server->nextPendingConnection();
   m_clients.Add(socket);
   emit ClientConnected(socket);
}

void MMPCore::Networking::Server::UpdateTimerTick()
{
   server->waitForNewConnection(100);
}
