//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef Linq_H
#define Linq_H

#include "LinqMacros.h"
#include "AbstractLinq.h"
#include "LinqStructs.h"
#include "BaseLinq.h"
#include "LinqSpecializations.h"
#include "LinqCreators.h"

#endif
