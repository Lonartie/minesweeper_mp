#pragma once

#include "stdafx.h"
#include "Serializable.h"

namespace MMPCore
{
   namespace Networking
   {
      class MMPCORE_EXPORT MessageQueue: public MMPCore::Serialization::Serializable
      {
         Q_OBJECT;
         MEMORY(MessageQueue);

      public:
         MessageQueue();
         ~MessageQueue();



      };

   }
}
