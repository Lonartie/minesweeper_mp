#include "FriendList.h"

MMPCore::Identity::FriendList::FriendList()
{}

MMPCore::Identity::FriendList::FriendList(const FriendList& fl) noexcept
{
   for (std::size_t i = 0; i < fl.m_numberOfFriends; i++)
      m_friends[i] = fl.m_friends[i];

   m_numberOfFriends = fl.m_numberOfFriends;
   CleanMemory();
}

MMPCore::Identity::FriendList::~FriendList()
{}

void MMPCore::Identity::FriendList::AddFriend(const MMPCore::Identity::Friend& fr)
{
   m_friends[m_numberOfFriends++] = fr;
}

void MMPCore::Identity::FriendList::RemoveFriend(const MMPCore::Identity::Friend& fr)
{
   auto index = Common::CreateLinq(std::vector<MMPCore::Identity::Friend>(m_friends, m_friends + m_numberOfFriends)).IndexOf(fr);
   m_friends[index] = Friend();
   CleanMemory();
   m_numberOfFriends--;
}

Common::Linq<MMPCore::Identity::Friend> MMPCore::Identity::FriendList::GetFriends() const
{
   return Common::CreateLinq(std::vector<MMPCore::Identity::Friend>(m_friends, m_friends + m_numberOfFriends));
}

bool MMPCore::Identity::FriendList::ContainsFriend(const MMPCore::Identity::Friend& fr) const
{
   for (std::size_t i = 0; i < m_numberOfFriends; i++)
      if (m_friends[i] == fr)
         return true;
   return false;
}

void MMPCore::Identity::FriendList::CleanMemory()
{
   std::vector<Friend> friends;

   for (std::size_t i = 0; i < MaxFriends; i++)
      if (m_friends[i] != Friend())
         friends.push_back(m_friends[i]);

   for (std::size_t i = 0; i < MaxFriends; i++)
      if (i < friends.size())
         m_friends[i] = friends[i];
      else
         m_friends[i] = Friend();
}
