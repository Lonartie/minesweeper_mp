#pragma once

// MMPCore
#include "stdafx.h"
#include "Friend.h"
#include "ReceiverMethod.h"

namespace MMPCore
{
   namespace Networking
   {
      class MMPCORE_EXPORT Client: public QObject
      {
         Q_OBJECT

      public:
         Client(QObject* parent = Q_NULLPTR);
         ~Client();

         bool Connect(const QString& host, unsigned port);

         QTcpSocket* socket;

      signals:

         void DataReceived(const QByteArray& data);

      private slots:

         void ReadData();

         void UpdateTimerTick();

      private:

         QTimer m_updateTimer;

      };

   }
}
