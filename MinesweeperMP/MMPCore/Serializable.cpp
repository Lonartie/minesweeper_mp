#include "Serializable.h"

QByteArray MMPCore::Serialization::Serializable::__Save() const
{
	Common::Linq<QByteArray> _data;
   auto hash = __TypeHash();
   _data.Add(QByteArray(static_cast<char*>(static_cast<void*>(&hash)), sizeof(std::size_t)));

   for (auto& kvp : __trivial_memory.Get())
   {
      _data.Add(QByteArray(static_cast<const char*>(kvp.first), kvp.second));
   }

	for (auto& ser : __recursive_memory.Get())
	{
		_data.Add(ser.first->__Save());
	}
	
	return _data.Accumulate(QByteArray(), [](auto& list, const auto& ref) { list.append(ref); });
}

bool MMPCore::Serialization::Serializable::__LoadFrom(const QByteArray& storage)
{
   if (!__CanLoadFrom(storage))
      return false;

   // make backup just in case
   QByteArray backup = __Save();

	std::size_t pos = sizeof(std::size_t);
	
   try
   {
      const char* data = storage.data();
      for (int i = 0; i < __trivial_memory.size(); i++)
      {
         auto& kvp = __trivial_memory[i];
         std::memcpy(kvp.first, data + pos, kvp.second);
         pos += kvp.second;
      }

      for (int i = 0; i < __recursive_memory.size(); i++)
      {
         auto& kvp = __recursive_memory[i];
         QByteArray meta(data + pos, kvp.second);
         kvp.first->__LoadFrom(meta);
         pos += kvp.second;
      }

      return true;
   }
   catch (...)
   {
      __LoadFrom(backup);
      return false;
   }
}

bool MMPCore::Serialization::Serializable::__CanLoadFrom(const QByteArray& storage) const
{
   std::size_t size =
      __trivial_memory.Select(&MemoryData::second).Sum() +
      __recursive_memory.Select(&SerializableData::second).Sum();

   auto ssize = storage.size() - sizeof(std::size_t);
   auto size_matches = ssize == size;

   auto hash = *static_cast<const std::size_t*>(static_cast<const void*>(storage.data()));
   auto mhash = __TypeHash();
   auto hash_matches = (hash == mhash);

   return size_matches && hash_matches;
}

MMPCore::Serialization::Serializable::~Serializable()
{
   __recursive_memory.Clear();
   __trivial_memory.Clear();
}
