#pragma once

template<typename T, typename First, typename ...Rest> struct is_one_of
{ enum { value = std::is_same<T, First>::value || is_one_of<T, Rest...>::value }; };
template<typename T, typename U> struct is_one_of<T, U>
{ enum { value = std::is_same<T, U>::value }; };

template<typename First, typename ...Rest>
struct FirstArg { using type = First; };

template<typename ...Fields> struct DynOb;

template<typename Copy, typename ...Tos>
void CopyField(DynOb<Tos...>& to, const Copy& from)
{
	if (is_one_of<Copy, Tos...>::value)
	{
		Copy& _to = dynamic_cast<Copy&>(to);
		_to.set(from);
	}
}

template<typename First, typename ...Rest, typename ...Tos, typename ...Froms, typename = typename std::enable_if<sizeof...(Rest) >= 1>::type>
void DynOb_CopyFields(DynOb<Tos...>& to, const First& obj, const DynOb<Froms...>& from)
{
	CopyField(to, obj);
	DynOb_CopyFields<Rest...>(to, from, from);
}

template<typename ...Rest, typename ...Tos, typename ...Froms, typename = typename std::enable_if<sizeof...(Rest) == 1>::type>
void DynOb_CopyFields(DynOb<Tos...>& to, const typename FirstArg<Rest...>::type& obj, const DynOb<Froms...>& from)
{
	CopyField(to, obj);
}

template<typename ...Fields>
struct DynOb: public Fields...
{
	DynOb() = default;
	virtual ~DynOb() = default;

	template<typename ...OtherFields>
	operator DynOb<OtherFields...>() const
	{
		DynOb<OtherFields...> _other;
		DynOb_CopyFields<Fields...>(_other, *this, *this);
		return _other;
	}

   template<typename ...OtherFields>
   DynOb<Fields...> operator+=(const DynOb<OtherFields...>& _other)
   {
      DynOb_CopyFields<OtherFields...>(*this, _other, _other);
      return *this;
   }

	template<typename ...OtherFields>
	DynOb<Fields...>& operator=(const DynOb<OtherFields...>& _other)
	{
      DynOb<Fields...> _this;
		DynOb_CopyFields<OtherFields...>(_this, _other, _other);
      *this = _this;
      return *this;
	}
};

#define FIELD(FT, FD) struct FD##Field{ FD##Field() = default; FT FD; virtual ~FD##Field() = default; void set(const FD##Field & obj) { FD = obj.FD; } }
