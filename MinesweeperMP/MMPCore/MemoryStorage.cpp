#include "MemoryStorage.h"

MMPCore::Mechanics::Storage::MemoryStorage::MemoryStorage(QObject* parent)
   : AbstractStorage(parent)
{

}

MMPCore::Mechanics::Storage::MemoryStorage::~MemoryStorage()
{

}

bool MMPCore::Mechanics::Storage::MemoryStorage::Save(const QString& key, const QByteArray& data)
{
   if (m_data.Where([&](auto& dat) { return dat.first == key; }).Any())
      return false;

   m_data.Add({key, data});
   return true;
}

void MMPCore::Mechanics::Storage::MemoryStorage::Remove(const QString& key)
{
   m_data.
      Remove(m_data.
             Where(L(x, x.first == key)).
             FirstOr({"",""}));
}

Common::Linq<QString> MMPCore::Mechanics::Storage::MemoryStorage::GetKeys() const
{
   return m_data.
      Select(&std::pair<QString, QByteArray>::first);
}

void MMPCore::Mechanics::Storage::MemoryStorage::Clear() noexcept
{
   m_data.Clear();
}

QByteArray MMPCore::Mechanics::Storage::MemoryStorage::Load(const QString& key) const
{
   return m_data.Where([&](auto& dat) { return dat.first == key; }).FirstOrDefault().second;
}