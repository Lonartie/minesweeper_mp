#pragma once

#include "stdafx.h"
#include "Serializable.h"
#include "SerializableString.h"

namespace MMPCore
{
   namespace Identity
   {
      class MMPCORE_EXPORT Friend : public MMPCore::Serialization::Serializable 
      {
         MEMORY(Friend);
         SERIALIZABLE_CLASS;

      public:

         Friend();
         Friend(const Friend& fr);
         ~Friend();

         bool operator==(const Friend& fr) const noexcept;
         bool operator!=(const Friend& fr) const noexcept;

         Friend& operator=(const Friend& fr) noexcept;
         Friend& operator=(Friend&& fr) noexcept;
         
         SERIALIZABLE(MMPCore::Serialization::Types::SerializableString<100>, name) = "generic";

      };

   }
}