#include "NetworkGame.h"

MMPCore::Mechanics::Game::NetworkGame::NetworkGame(const MMPCore::Identity::Player& initiator, const Common::Linq<MMPCore::Identity::Player>& contestants)
   : m_rulesController(initiator, contestants)
{

}

const MMPCore::Mechanics::Game::Board& MMPCore::Mechanics::Game::NetworkGame::GetBoard() const
{
   return m_board;
}

const MMPCore::Mechanics::Game::GameRulesController& MMPCore::Mechanics::Game::NetworkGame::GetRulesController() const
{
   return m_rulesController;
}

std::size_t MMPCore::Mechanics::Game::NetworkGame::GetGUID() const
{
   return m_gameGuid;
}
