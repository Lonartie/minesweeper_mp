#pragma once

#include "stdafx.h"
#include "Client.h"
#include "ReceiverMethod.h"

namespace MMPCore
{
   namespace Networking
   {
      class MMPCORE_EXPORT GameClient: public Client
      {
         Q_OBJECT

      public:
         GameClient(QObject* parent = Q_NULLPTR);
         ~GameClient() = default;

         void AddReceiverMethods(const Common::Linq<ReceiverMethod>& methods);

         /// @brief           connecting to a server with given \a address
         /// @note            wrapping the client method just with fixed port (22552)
         /// @param  address  the address of the server
         /// @returns         whether or not the connection was successful
         bool Connect(const QString& address);

      private slots:

         void _DataReceived(const QByteArray& data);

      private:

         MMPCore::Identity::Friend m_connectedTo;

         Common::Linq<ReceiverMethod> m_methods;

      };
   }
}