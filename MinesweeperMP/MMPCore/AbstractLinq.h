//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef AbstractLinq_H
#define AbstractLinq_H

#include <vector>

namespace Common
{
   template<typename T>
   struct Linq;

   template<typename T>
   struct LinqData
   {
      /*
         ##################### Member Variables #####################
      */

      virtual std::vector<T>& List() = 0;
      virtual const std::vector<T>& ConstList() const = 0;
      virtual Linq<T>& This() = 0;
      virtual const Linq<T>& ConstThis() const = 0;
      virtual ~LinqData() {};
   };
}

#endif
