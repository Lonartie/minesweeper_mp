#include "Client.h"

MMPCore::Networking::Client::Client(QObject* parent /*= Q_NULLPTR*/)
   : QObject(parent)
   , socket(new QTcpSocket(this))
{
   m_updateTimer.start();

   connect(&m_updateTimer, SIGNAL(timeout()), this, SLOT(UpdateTimerTick()));
   connect(socket, SIGNAL(readyRead()), this, SLOT(ReadData()));
}

MMPCore::Networking::Client::~Client()
{
   m_updateTimer.stop();
}

bool MMPCore::Networking::Client::Connect(const QString& host, unsigned port)
{
   socket->connectToHost(host, static_cast<quint16>(port));
   return socket->waitForConnected();
}

void MMPCore::Networking::Client::ReadData()
{
   emit DataReceived(socket->readAll());
}

void MMPCore::Networking::Client::UpdateTimerTick()
{
   socket->waitForReadyRead(100);
}
