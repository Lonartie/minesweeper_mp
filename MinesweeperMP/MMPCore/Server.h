#pragma once

// MMPCore
#include "stdafx.h"
#include "Friend.h"

namespace MMPCore
{
   namespace Networking
   {
      class MMPCORE_EXPORT Server: public QObject
      {
         Q_OBJECT

      public:

         Server(unsigned port, QObject* parent = Q_NULLPTR);
         ~Server();

         QTcpServer* server;

         void BroadcastData(const QByteArray& data);

      signals:

         void ClientConnected(QTcpSocket* socket);

      public slots:

         void SendData(QTcpSocket* socket, const QByteArray& data);

         void NewConnection();

      private slots:

         void UpdateTimerTick();

      private:

         QTimer m_updateTimer;

         Common::Linq<QTcpSocket*> m_clients;

      };
   }
}