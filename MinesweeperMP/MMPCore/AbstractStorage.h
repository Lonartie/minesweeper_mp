#pragma once

#include "stdafx.h"
#include "Serializable.h"

#include <QString>

namespace MMPCore
{
   namespace Mechanics
   {
      namespace Storage
      {
         class AbstractStorage: public QObject
         {
            Q_OBJECT;
            MEMORY(AbstractStorage);

         public:

            AbstractStorage(QObject* parent = Q_NULLPTR): QObject(parent) {};
            virtual ~AbstractStorage();
            bool Save(const QString& key, const MMPCore::Serialization::Serializable& data);
            bool CopyTo(AbstractStorage& storage) const;
            bool CopyFrom(const AbstractStorage& storage);
            bool WriteTo(MMPCore::Serialization::Serializable& out, const QString& key) const;
            bool KeyExists(const QString& key) const;

         public /*templates*/:

            template<typename T, typename ...Args>
            T* ConstructPtr(const QString& key, Args...args) const;
            template<typename T, typename ...Args>
            T Construct(const QString& key, Args...args) const;

         public /*abstract*/:
            virtual bool Save(const QString& key, const QByteArray& data) = 0;
            virtual void Remove(const QString& key) = 0;
            virtual QByteArray Load(const QString& key) const = 0;
            virtual Common::Linq<QString> GetKeys() const = 0;
            virtual void Clear() noexcept = 0;

         };

         inline AbstractStorage::~AbstractStorage()
         {}

         inline bool AbstractStorage::Save(const QString& key, const MMPCore::Serialization::Serializable& data)
         {
            return Save(key, data.__Save());
         }

         inline bool AbstractStorage::CopyTo(AbstractStorage& storage) const
         {
            auto keys = GetKeys();
            for (auto& key : keys)
               if (!storage.Save(key, Load(key)))
                  return false;
            return true;
         }

         inline bool AbstractStorage::CopyFrom(const AbstractStorage& storage)
         {
            auto keys = storage.GetKeys();
            for (auto& key : keys)
               if (!Save(key, storage.Load(key)))
                  return false;
            return true;
         }

         inline bool AbstractStorage::WriteTo(MMPCore::Serialization::Serializable& out, const QString& key) const
         {
            auto dat = Load(key);
            if (!out.__CanLoadFrom(dat)) return false;
            return out.__LoadFrom(dat);
         }

         inline bool AbstractStorage::KeyExists(const QString& key) const
         {
            return GetKeys().Contains(key);
         }

         template<typename T, typename ...Args>
         inline T* MMPCore::Mechanics::Storage::AbstractStorage::ConstructPtr(const QString& key, Args...args) const
         {
            T* _new = new T(args...);
            auto _dat = Load(key);
            if (!T().__CanLoadFrom(_dat)) return nullptr;
            _new->__LoadFrom(_dat);
            return _new;
         }

         template<typename T, typename ...Args>
         inline T MMPCore::Mechanics::Storage::AbstractStorage::Construct(const QString& key, Args...args) const
         {
            if constexpr (sizeof...(args) == 0)
            {
               T _new;
               auto _dat = Load(key);
               if (!T().__CanLoadFrom(_dat)) return T();
               _new.__LoadFrom(_dat);
               return _new;
            } else
            {
               T _new(args...);
               auto _dat = Load(key);
               if (!T(args).__CanLoadFrom(_dat)) return T(args);
               _new.__LoadFrom(_dat);
               return _new;
            }
         }
      }
   }
}