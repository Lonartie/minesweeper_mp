//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef CharLinq_H
#define CharLinq_H

#include "LinqSpecializationMaker.h"

namespace Common
{
   /*
      ##################### Specialization group #####################
   */

   LINQ_GROUP_BEGIN(CharLinq);

   /// @brief 							get the elements as QString
   /// @note  							QChar must be constructible from T
   QString ToQString() const
   {
      static_assert(std::is_constructible<QChar, T>::value, "QChar must be constructible from T!");
      auto SumAccumulator = [](auto& str, const auto& ch) { str += QChar(ch); };
      return this->ConstThis().Accumulate(QString(), SumAccumulator);
   }

   LINQ_GROUP_END(CharLinq);

   /*
      ##################### Specializations #####################
   */

   LINQ_SPECIALIZATION(QChar, CharLinq);
}
#endif
