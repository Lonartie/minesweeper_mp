#pragma once

#include "stdafx.h"
#include "Serializable.h"
#include "Utils.h"
#include "GameRulesController.h"
#include "Flags.h"

namespace MMPCore
{
   namespace Mechanics
   {
      namespace Game
      {
         class MMPCORE_EXPORT NetworkGame: public MMPCore::Serialization::Serializable
         {
            SERIALIZABLE_CLASS;
            MEMORY(NetworkGame);

         public /*static*/:

            static constexpr std::size_t MaxFlags = MMPCore::Mechanics::Game::GameRulesController::MaxNumberOfContestants;

         public /*construction*/:

            NetworkGame(const MMPCore::Identity::Player& initiator,
                        const Common::Linq<MMPCore::Identity::Player>& contestants);
            NetworkGame() = default;
            ~NetworkGame() = default;

         public /*methods*/:

            const MMPCore::Mechanics::Game::Board& GetBoard() const;
            const MMPCore::Mechanics::Game::GameRulesController& GetRulesController() const;
            std::size_t GetGUID() const;

         private /*methods*/:

         private /*members*/:

            SERIALIZABLE(std::size_t, m_gameGuid) = MMPCore::Utils::RandomGUID();
            SERIALIZABLE(MMPCore::Mechanics::Game::GameRulesController, m_rulesController);
            SERIALIZABLE(MMPCore::Mechanics::Game::Board, m_board);
            SERIALIZABLE_ARRAY(MMPCore::Mechanics::Game::Flags, m_allFlags, MaxFlags);
         };
      }
   }
}

