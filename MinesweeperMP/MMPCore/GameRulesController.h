#pragma once

#include "stdafx.h"
#include "Player.h"
#include "Serializable.h"
#include "Board.h"

namespace MMPCore
{
	namespace Mechanics
	{
		namespace Game
		{
			class MMPCORE_EXPORT GameRulesController: public MMPCore::Serialization::Serializable
			{
            SERIALIZABLE_CLASS;
            MEMORY(GameRulesController);

			public:

            static constexpr std::size_t MaxNumberOfContestants = 8;

            GameRulesController(const MMPCore::Identity::Player& initiator,
                                const Common::Linq<MMPCore::Identity::Player>& contestants,
										  QObject *parent = Q_NULLPTR);

				GameRulesController() = default;

				virtual ~GameRulesController();

				bool MoveAllowed(std::size_t x, std::size_t y, 
                             const MMPCore::Identity::Player& player,
                             const MMPCore::Mechanics::Game::Board& board);

            MMPCore::Identity::Player GetCurrentPlayer() const;
            MMPCore::Identity::Player GetLastPlayer() const;
            MMPCore::Identity::Player GetNextPlayer() const;
            void PlayerMadeMove();

				const MMPCore::Identity::Player& GetInitiator() const;
            Common::Linq<MMPCore::Identity::Player> GetContestants() const;

			private:

				SERIALIZABLE(MMPCore::Identity::Player, m_initiator);
				SERIALIZABLE(std::size_t, m_numberOfContestants) = 0;
            SERIALIZABLE(std::size_t, m_currentPlayerID) = 0;
            SERIALIZABLE(std::size_t, m_nextPlayerID) = 0;
            SERIALIZABLE(std::size_t, m_lastPlayerID) = 0;
				SERIALIZABLE_ARRAY(MMPCore::Identity::Player, m_contestants, MaxNumberOfContestants);

			};

		}
	}
}
