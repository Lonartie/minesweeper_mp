//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#ifndef QStringLinq_H
#define QStringLinq_H

#include "LinqSpecializationMaker.h"

#include <QtCore/QString>
#include <QtCore/QStringList>

namespace Common
{
	/*
		##################### Specialization group #####################
	*/

    namespace
    {
        template<typename U> inline U To(const QString& str) { return U{}; }
        template<> inline int To<int>(const QString& str) { return str.toInt(); }
        template<> inline double To<double>(const QString& str) { return str.toDouble(); }
        template<> inline float To<float>(const QString& str) { return str.toFloat(); }
    }

	LINQ_GROUP_BEGIN(QStringLinq);

	/// @brief 							get the elements as QStringList
	QStringList ToQStringList() const
	{
		return this->ConstThis().Accumulate(QStringList(), &QStringLinq::QStringListAccumulator);
	}

	/// @brief 							sums up a QString from begin to end
	QString Concatenate() const
	{
		return this->ConstThis().Accumulate(QString(), &QStringLinq::QStringSum, "", false);
	}

	/// @brief 							sums up a QString from begin to end with given \a separator
	QString ConcatenateWithSeparator(const QString& separator, bool exclude_first = true) const
	{
		return this->ConstThis().Accumulate(QString(), &QStringLinq::QStringSum, separator, exclude_first);
	}

	Linq<int> ToIntLinq() const
	{
        return this->ConstThis().Select(&To<int>);
	}

	Linq<double> ToDoubleLinq() const
	{
        return this->ConstThis().Select(&To<double>);
	}

	Linq<float> ToFloatLinq() const
	{
        return this->ConstThis().Select(&To<float>);
	}

private:

	static void QStringListAccumulator(QStringList& list, const QString& str)
	{
		list.append(str);
	}

	static void QStringSum(QString& out, const QString& item, const QString& separator, bool exclude_first)
	{
		out += (out.isEmpty() && exclude_first ? "" : separator) + item;
    }

	LINQ_GROUP_END(QStringLinq);

	/*
		##################### Specializations #####################
	*/

	LINQ_SPECIALIZATION(QString, QStringLinq);
}

#endif
