//-------------------------------------------------------------------------------------------------
//
// Copyright (C) 2019 LaVision GmbH.  All Rights Reserved.
//
//-------------------------------------------------------------------------------------------------

#ifndef OldStringLinq_H
#define OldStringLinq_H

#include "LinqSpecializationMaker.h"

#include <QString>

#include <string>

namespace Common
{
   /*
      ##################### Specialization group #####################
   */

   LINQ_GROUP_BEGIN(OldStringLinq);

   /// @brief 							creates a Linq of QString from a Linq of old string types
   Linq<QString> ToQStringLinq() const
   {
      return this->ConstThis().Select(&OldStringLinq<T>::ToQString);
   }

private:

   static QString ToQString(const T str)
   {
      return Transform(str);
   }

   static QString Transform(const char* str)
   {
      return QString::fromLatin1(str);
   }

   static QString Transform(const std::string& str)
   {
      return QString::fromStdString(str);
   }

   LINQ_GROUP_END(OldStringLinq);

   /*
      ##################### Specializations #####################
   */

   LINQ_SPECIALIZATION(char*, OldStringLinq);
   LINQ_SPECIALIZATION(const char*, OldStringLinq);
   LINQ_SPECIALIZATION(std::string, OldStringLinq);
}

#endif
