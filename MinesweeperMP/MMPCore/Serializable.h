#pragma once

#include "stdafx.h"
#include "Linq.h"

namespace MMPCore
{
	namespace Serialization
	{
		class Serializable;
		struct MemoryData
		{
			void* first;
			std::size_t second;
		};

		struct SerializableData
		{
			Serializable* first;
			std::size_t second;
		};

      struct PointerData
      {
         void* first;
         std::size_t second;
      };

		class MMPCORE_EXPORT Serializable: public QObject
		{
			Q_OBJECT;
			MEMORY(Serializable);

		public:

         virtual std::size_t __TypeHash() const = 0;

			virtual ~Serializable();

			template<typename T>
			static bool RegisterType(Serializable* serializable, 
                                  T* value, 
                                  std::size_t length = -1);

			QByteArray __Save() const;
			bool __LoadFrom(const QByteArray& storage);
         bool __CanLoadFrom(const QByteArray& storage) const;

		protected:

			Common::Linq<MemoryData> __trivial_memory;
			Common::Linq<SerializableData> __recursive_memory;
		};

		template<typename T>
		inline bool MMPCore::Serialization::Serializable::RegisterType(Serializable* serializable, 
                                                                     T* value, 
                                                                     std::size_t length /*= -1*/)
		{
         if constexpr (std::is_base_of<Serializable, T>::value)
         {
            std::size_t _size = T().__Save().size();
            for (std::size_t i = 0; i < length; i++)
            {
               auto ptr = static_cast<Serializable*>(value + i);
               serializable->__recursive_memory.Add({ptr, _size});
            }
         } 
         else
         {
            serializable->__trivial_memory.Add({value, length * sizeof(decltype(*value))});
         }
			return true;
		}
	}
}


#define SERIALIZABLE(type, name) 									                  \
bool __##name##_registered = 											                  \
	MMPCore::Serialization::Serializable::RegisterType(this,                   \
																		&name,                  \
																		1); 	                  \
type name



#define SERIALIZABLE_ARRAY(type, name, size)						                  \
bool __##name##_registered = 											                  \
	MMPCore::Serialization::Serializable::RegisterType(this,                   \
																		name,                   \
																		size);                  \
type name[size]



#define SERIALIZABLE_CLASS                                                    \
public:                                                                       \
   virtual std::size_t __TypeHash() const override                            \
   {                                                                          \
      return std::hash<std::string>{}(typeid(this).name());                   \
   }                                                                          \
private: