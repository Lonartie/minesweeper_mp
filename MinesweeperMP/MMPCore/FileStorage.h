#pragma once

#include "stdafx.h"
#include "AbstractStorage.h"

namespace MMPCore
{
	namespace Mechanics
	{
		namespace Storage
		{
			class MMPCORE_EXPORT FileStorage: public MMPCore::Mechanics::Storage::AbstractStorage
			{
				Q_OBJECT;
				MEMORY(FileStorage);

			public:
            using AbstractStorage::Save;
				FileStorage();
				virtual ~FileStorage();

            void SetOverrideMode(bool mode = false);
            bool RecoverToMemory(const QString& key);

         public /*virtual*/:

				virtual bool Save(const QString& key, const QByteArray& data) override;
            virtual void Remove(const QString& key) override;
				virtual QByteArray Load(const QString& key) const override;
            virtual Common::Linq<QString> GetKeys() const override;
            virtual void Clear() noexcept override;

         private:

            bool SaveInMemory(const QString& key, const QByteArray& data);
            bool SaveOnDrive(const QString& key, const QByteArray& data);

            bool KeyExistsOnDrive(const QString& key) const;
            bool KeyExistsInMemory(const QString& key) const;
            bool KeyExists(const QString& key) const;

            bool RemoveInMemory(const QString& key);
            bool RemoveOnDrive(const QString& key);
            bool RemoveBoth(const QString& key);

            QByteArray LoadDataFromFile(const QString& path) const;

            QString GetPathOfKey(const QString& key) const;

            bool m_override = false;

				Common::Linq<std::pair<QString /*key*/, QString /*path*/>> m_data;

			};
		}
	}
}
