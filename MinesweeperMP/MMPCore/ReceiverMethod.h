#pragma once

#include "stdafx.h"
#include "Serializable.h"

namespace MMPCore
{
   namespace Networking
   {
      struct ReceiverMethod
      {
         std::size_t TypeHash = 0;
         std::function<void(const MMPCore::Identity::Friend&, MMPCore::Serialization::Serializable*)> method;
         std::function<void(const MMPCore::Identity::Friend&, const QByteArray&)> caller;
      };

      template<typename T, typename ...Args>
      inline ReceiverMethod CreateReceiver(Args...args, std::function<void(const MMPCore::Identity::Friend&, T*)> method)
      {
         static_assert(std::is_base_of<MMPCore::Serialization::Serializable, T>::value, "T must inherit from MMPCore::Serilization::Serializable!");

         auto _hash = T(args...).__TypeHash();
         auto _method = [method](const auto& a, auto* b) { method(a, static_cast<T*>(b)); };
         auto _caller = [_method, args...](const auto& a, const auto& b) { T* _T = new T(args...); _T->__LoadFrom(b); _method(a, _T); };

         return ReceiverMethod { _hash, _method, _caller };
      }
   }
}