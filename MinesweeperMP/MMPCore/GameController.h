#pragma once

#include "stdafx.h"
#include "Friend.h"
#include "Serializable.h"
#include "FriendList.h"
#include "Server.h"
#include "ReceiverMethod.h"
#include "Linq.h"
#include "Player.h"
#include "Client.h"

namespace MMPCore
{
   namespace Mechanics
   {
      namespace Game
      {
         class GameController: public MMPCore::Serialization::Serializable
         {
            Q_OBJECT;
            SERIALIZABLE_CLASS;

         private slots:

            void AcceptOrDenyPlayerConnection(MMPCore::Identity::Friend fr);

         public:
            GameController(QObject* parent = Q_NULLPTR);
            ~GameController();

         private /*Network Receivers*/:

            void PlayerDataSent(const MMPCore::Identity::Friend& sender, MMPCore::Identity::Player* player);
            void FriendDataSent(const MMPCore::Identity::Friend& sender, MMPCore::Identity::Friend* fr);
            void FriendListDataSent(const MMPCore::Identity::Friend& sender, MMPCore::Identity::FriendList* fr);

         private:

            Common::Linq<MMPCore::Networking::ReceiverMethod> CreateReceiverMethods();

            void ConnectSignals();

            MMPCore::Networking::Server m_server;
            MMPCore::Networking::Client m_client;

            SERIALIZABLE(MMPCore::Identity::FriendList, m_friendList);

         };

      }
   }
}