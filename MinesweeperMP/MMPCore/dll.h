#pragma once

#include "Linq.h"

#ifndef BUILD_STATIC
# if defined(MMPCORE_LIB)
#  define MMPCORE_EXPORT __declspec(dllexport)
# else
#  define MMPCORE_EXPORT __declspec(dllimport)
# endif
#else
# define MMPCORE_EXPORT
#endif