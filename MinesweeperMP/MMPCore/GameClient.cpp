#include "GameClient.h"

MMPCore::Networking::GameClient::GameClient(QObject* parent /*= Q_NULLPTR*/)
{
   connect(this, SIGNAL(DataReceived(const QByteArray&)), this, SLOT(_DataReceived(const QByteArray&)));
}

void MMPCore::Networking::GameClient::AddReceiverMethods(const Common::Linq<ReceiverMethod>& methods)
{
   m_methods = methods;
}

bool MMPCore::Networking::GameClient::Connect(const QString& address)
{
   return Client::Connect(address, 22552);
}

void MMPCore::Networking::GameClient::_DataReceived(const QByteArray& data)
{
   if (data.size() <= sizeof(std::size_t))
      return;

   REQUIRES(data.size() > sizeof(std::size_t));
   
   m_methods.
      Where(lambda(x, x.TypeHash == *static_cast<const std::size_t*>(static_cast<const void*>(data.data())))).
      Foreach(lambda(x, x.caller(m_connectedTo, data)));
}

