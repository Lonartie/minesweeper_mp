#pragma once

#include "stdafx.h"

#include "ui_BoardWidget.h"
#include "../MMPCore/Board.h"

class BoardWidget : public QWidget
{
	Q_OBJECT

public:
	BoardWidget(QWidget *parent = Q_NULLPTR);
	~BoardWidget();

public slots:

	void UpdateBoard(const MMPCore::Mechanics::Game::Board& board) const;

private:

	Ui::BoardWidget m_ui;
};
