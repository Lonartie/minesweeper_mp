#include "BoardWidget.h"
using namespace MMPCore::Mechanics::Game;

BoardWidget::BoardWidget(QWidget *parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);
}

BoardWidget::~BoardWidget()
{
}

void BoardWidget::UpdateBoard(const Board& board) const
{
	QImage image(255, 255, QImage::Format_RGB888);
	QPainter painter(&image);

	std::size_t width = board.GetBoardWidth();
	std::size_t height = board.GetBoardHeight();
	double w_factor = 255.0 / width;
	double h_factor = 255.0 / height;

	for (std::size_t x = 0; x < width; x++)
	{
		for (std::size_t y = 0; y < height; y++)
		{
			if (board.FieldStateContains(x, y, Board::HAS_MINE))
			{
				painter.setBrush(QBrush(Qt::red));
			}
			else
			{
				painter.setBrush(QBrush(Qt::green));
			}

			painter.drawRect(x * w_factor, y * h_factor, w_factor, h_factor);
			painter.drawText(QRect(x * w_factor, y * h_factor, w_factor, h_factor),
								  QString::number(board.CountMines(x, y)),
								  QTextOption(Qt::AlignCenter));
		}
	}

	m_ui.content->setPixmap(QPixmap::fromImage(image));
}
