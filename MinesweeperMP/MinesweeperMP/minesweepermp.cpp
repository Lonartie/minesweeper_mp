// MinesweeperMP
#include "stdafx.h"
#include "minesweepermp.h"

// MMPCore
#include "../MMPCore/AbstractStorage.h"
#include "../MMPCore/MemoryStorage.h"
#include "../MMPCore/Player.h"
#include "../MMPCore/MinesController.h"
#include "../MMPCore/GameRulesController.h"
#include "../MMPCore/SerializableString.h"
#include "../MMPCore/Friend.h"
#include "../MMPCore/Server.h"
#include "../MMPCore/FriendList.h"
#include "../MMPCore/FileStorage.h"
#include "../MMPCore/ReceiverMethod.h"
#include "../MMPCore/Serializable.h"
#include "../MMPCore/Client.h"
#include "../MMPCore/CommonRequests.h"
#include "../MMPCore/NetworkGame.h"

#include <chrono>
#include <time.h>

using namespace MMPCore::Mechanics::Storage;
using namespace MMPCore::Mechanics::Game;
using namespace MMPCore::Identity;
using namespace MMPCore::Serialization::Types;
using namespace MMPCore::Serialization;
using namespace MMPCore::Networking;

MinesweeperMP::MinesweeperMP(QWidget* parent)
   : QMainWindow(parent)
   , m_board(64, 64)
{
   srand(time(0));

   // TESTS

   MinesController controller(MinesController::Felix);
   controller.InitBoard(m_board);
   m_boardWidget.UpdateBoard(m_board);

   m_ui.setupUi(this);

   connect(m_ui.newBoard, &QPushButton::pressed, this, [&]()
   {
      MinesController controller(MinesController::Felix);
      controller.InitBoard(m_board);
      m_boardWidget.UpdateBoard(m_board);
   });

   m_ui.layout->addWidget(&m_boardWidget);

}