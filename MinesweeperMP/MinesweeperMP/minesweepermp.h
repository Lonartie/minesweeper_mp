#pragma once

#include "stdafx.h"
#include "ui_minesweepermp.h"
#include "../MMPCore/Board.h"
#include "BoardWidget.h"

class MinesweeperMP : public QMainWindow
{
	Q_OBJECT;
	MEMORY(MinesweeperMP);

public:
	MinesweeperMP(QWidget *parent = Q_NULLPTR);

private:

	MMPCore::Mechanics::Game::Board m_board;
	BoardWidget m_boardWidget;

	Ui::MinesweeperMPClass m_ui;
};